<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index.html', function () {
    return view('index');
});

Route::get('cars.html', function () {
    return view('cars');
});

Route::get('contact.html', function () {
    return view('contact');
});

Route::get('services.html', function () {
    return view('services');
});

Route::get('single.html', function () {
    return view('single');
});

Route::get('about.html', function () {
    return view('about');
});

Route::get('blog.html', function () {
    return view('blog');
});

Route::get('iznajmljivanjeVozila.html', function () {
    return view('iznajmljivanjeVozila');
});

Route::get('detaljnije.html', function () {
    return view('detaljnije');
});

Route::get('registrovanje.html', function () {
    return view('registrovanje');
});

Route::get('prijavljivanje.html', function () {
    return view('prijavljivanje');
});

Route::get('servis.html', function () {
    return view('servis');
});

Route::get('profil.html', function () {
    return view('profil');
});

Route::get('perionica.html', function () {
    return view('perionica');
});

Route::get('serviser.html', function () {
    return view('serviser');
});

Route::get('radnikUperionici.html', function () {
    return view('radnikUperionici');
});

Route::get('oNama.html', function () {
    return view('oNama');
});

Route::get('add','CarController@create');
Route::post('add','CarController@store');
Route::get('car','CarController@index');
Route::get('edit/{id}','CarController@edit');
Route::post('edit/{id}','CarController@update');
//Route::delete('{id}','CarController@destroy');

///Jovan Krstic /////
//Prijava i registracija
Route::post('/registrovanje','KorisnikController@registracijaNovogKorisnika');
Route::post('/prijavljivanje','KorisnikController@prijavljivanjeKorisnika');
//Iznajmljivanje
Route::post('/vrativozilazaiznajmljivanje','VoziloZaIznajmljivanjeController@vratiListuSvihVozila');

//Profil
Route::post("/novovozilokorisnika","KorisnikController@dodajNovoVozilo");
Route::post("/vrativozilojednogkorisnika","KorisnikController@vratiSvaVozilaJednogKorisnika");
Route::post("/brisanjevozilakorisnika","KorisnikController@obrisiVoziloIzGaraze");
Route::post("/vratisveokorisniku","KorisnikController@vratiSveInformacijeOKorisnikuPremaEmailAdresi");
Route::post("/listarezervacijajednogkorisnika","ZakazivanjeController@izlistajSveRezervacije");
Route::post("/servisnaistorijajednogvozila","KorisnikController@vratiServisnuIstorijuJednogVozila");
Route::post("/profilzakzaivanjepranja","ZakazivanjeController@zakaziPranje");
Route::post("/profilzakazivanjeservisa","ZakazivanjeController@zakaziServisiranje");


///Test Kontroler
Route::get('/testiranjejox', 'TestControllerJovan@testFunkcija');

/////Katarina Krsticc///
Route::get('/testiranjeModela', 'TestController@testFunkcija');

//Cenovnik servisa
Route::post('/izlistavanjeCenaRadova', 'CenovnikController@vratiUslugeZaServisPremaOdabranojKategoriji');
Route::post('/izlistavanjeCenovnikaPerionice', 'CenovnikController@vratiSveUslugeZaPranje');

//Filtriranje vozila za iznajmljivanje
Route::post('/filtriranjeVozilaZaIznajmljivanje', 'VoziloZaIznajmljivanjeController@filtriranjeVozilaZaIznajmljivanje');
Route::get('/detaljniPrikazVozila{a}', 'VoziloZaIznajmljivanjeController@vratiVoziloZaIznajmljivanje');
Route::post('/iznajmiVozilo', 'VoziloZaIznajmljivanjeController@dodajRezervacijuZaIznajmljivanje');

//Radnik u perionici
Route::post('/dodajNovuRezervacijuZaPranje', 'ZakazivanjeController@zakaziPranjeKaoRadnik');
Route::post('/listanjeSvihZakazanihPranja', 'ZakazivanjeController@izlistajSveRezervacijeZaPranje');
Route::post('/operiKola', 'ZakazivanjeController@obrisiVoziloIzListeZakazanihVozilaZaPerionicu');

//Serviser
Route::post('/izlistavanjeSvihZakazanihRezervacijaUServisu', 'ZakazivanjeController@izlistajSveRezervacijeZaServis');
Route::post('/dodavanjeNoveRezervacije', 'ZakazivanjeController@zakaziServisiranjeKaoRadnik');
Route::post('/azuriranjeServisneIstorijeVozila', 'KorisnikController@dodajServisnuIstorijuVozila');
Route::post('/obrisiVozilo', 'ZakazivanjeController@obrisiVoziloZaServisiranje');

//Promena prijave u odjavu
Route::post('/proveravanjeDaLiJeKorisnikPrijavljen', 'PromenaPrijaveUOdjavuController@proveravanjeDaLiJeKorisnikPrijavljen');
Route::post('/odjaviSe', 'PromenaPrijaveUOdjavuController@odjaviSe');
