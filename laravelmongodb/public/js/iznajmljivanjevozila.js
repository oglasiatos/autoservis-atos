(function ($) {
    "use strict";


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(document).ready(function(){
		$.ajax({
            method: "POST",
            url: Settings.url_vrativozilazaiznajmljivanje,
            success: function(data)
            {
                console.log(data);
                NacrtajVozilo(data);
            },
            dataType: "json"
    	});

	});

	function NacrtajVozilo(vozila)
	{
		$(".listavozila").empty();
		$.each(vozila, function( index, value ) {
		    var s="<div class='col-lg-4 col-md-6 mb-4'><div class='item-1'><a href='#'><img src='images/img_1.jpg' alt='Image' class='img-fluid'></a><div class='item-1-contents'><div class='text-center'>";
		    s+="<h3><a href='#'>"+value["naziv_vozila"]+"</a></h3><div class='rent-price'><span>"+value["cena_po_danu"]+"</span>€ po danu</div>";
		    s+="</div><ul class='specs'><li><span>Broj vrata</span><span class='spec'>"+value["broj_vrata"]+"</span></li><li><span>Broj sedišta</span><span class='spec'>"+value["broj_sedista"]+"</span>";
			s+="</li><li><span>Vrsta menjača</span><span class='spec'>"+value["automatski_ili_rucni_menjac"]+"</span></li><li>";
			s+="<span>Vrsta goriva</span><span class='spec'>"+value['vrsta_goriva']+"</span></li></ul>";	
			s+=" <div class='d-flex action justify-content-center'><a href='"+Settings.detaljniPrikazVozila_url+value['id']+"' class='btn btn-primary'>Detaljnije</a>";
			s+="</div></div></div></div>";	

			$(".listavozila").append(s);

		});
 		/*
<div class="col-lg-4 col-md-6 mb-4"><div class="item-1"><a href="#"><img src="images/img_1.jpg" alt="Image" class="img-fluid"></a><div class="item-1-contents"><div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3><div class="rent-price"><span>250</span>€ po danu</div>
                  </div><ul class="specs"><li><span>Broj vrata</span><span class="spec">4</span></li><li><span>Broj sedišta</span><span class="spec">5</span>
                  </li><li><span>Vrsta menjača</span><span class="spec">Automatski</span></li><li>
                  <span>Vrsta goriva</span><span class="spec">Benzin</span></li></ul>
                  <div class="d-flex action justify-content-center"><a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div></div></div></div>
              */
	};

  var uredjenjeCene = {};
  uredjenjeCene['cena'] = "Rastuca";
  
  $('.filtriranjeCeneRastuca').click(function(){
      uredjenjeCene['cena'] = "Rastuca";
      prikupljanjeInformacijaZaFiltriranje();
  });

  $('.filtriranjeCeneOpadajuca').click(function(){
      uredjenjeCene['cena'] = "Opadajuca";
      prikupljanjeInformacijaZaFiltriranje();
  })


  $('.filtriranjeVozila').click(function(){
      console.log("Filtriranje vozila za iznajmljivanje!");
      prikupljanjeInformacijaZaFiltriranje();
  });

  function prikupljanjeInformacijaZaFiltriranje()
  {
      var input = $('.filtriranje');
      var stringArray = {};
      for(var i = 0; i < input.length; i++)
      {
          if($(input[i]).attr('name')==="automatski_ili_rucni_menjac")
          {
              stringArray[$(input[i]).attr('name')] = $("[name='"+$(input[i]).attr('name')+"']:checked").val();  
          }
          else if($(input[i]).val() === null)
          {
              stringArray[$(input[i]).attr('name')] = "";
          }
          else
          {
            if($(input[i]).attr('name') === "broj_sedista" || $(input[i]).attr('name') === "broj_vrata")
            {
              stringArray[$(input[i]).attr('name')] = parseInt($(input[i]).val());
            }
            else
            {
              stringArray[$(input[i]).attr('name')] = $(input[i]).val();
            }
          }
      }
      stringArray['cena'] = uredjenjeCene['cena'];
      console.log(stringArray);
      $.ajax({
          method: "POST",
          url: Settings.filtriranjeVozilaZaIznajmljivanje_url,
          data: stringArray,
          success: function(data){
            console.log(data);
            NacrtajVozilo(data);
          },
          dataType: "json"
      });
  }

})(jQuery);