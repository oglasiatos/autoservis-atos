(function ($) {
    "use strict";


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(document).ready(function(){
		$( "form" ).submit(function( event ){ 
			var ulazi=$(".form-control");
	    	var niz={};
	    	$.each(ulazi, function( index, value ) {
			   console.log(value.value.length);
			   niz[value.name]=value.value;
			});
	    	console.log(niz);
			$.ajax({
	            method: "POST",
	            url: Settings.url_registracija,
	            data:niz,
	            success: function(data)
	            {
	                console.log(data);
	                if(data['Status']==="Prijavljivanje korisnika nije uspelo!")
	                {
	                	alert("Problem sa prijavljivanjem pogresna lozinka ili e-mail adresa!!!");
	                }
	                else if(data['Status'] === "Prijavljivanje korisnika je uspelo!")
	                {		
	                	window.location.replace(Settings.url_pocetna);
	                }
	                else if(data['Status'] === "Prijavljivanje servisera je uspelo!")
	                {
	                	window.location.replace(Settings.stranicaServisera_url);
	                }
	                else
	                {
	                	window.location.replace(Settings.stranicaPeraca_url);
	                }

	                
	            },
	            dataType: "json"
        	});

		});
    });



})(jQuery);