(function ($) {
    "use strict";


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(document).ready(function(){
		$( "form" ).submit(function( event ){ 
			var ulazi=$(".form-control");
	    	var niz={};
	    	$.each(ulazi, function( index, value ) {
			   console.log(value.value.length);
			   if(value.id==="email")
			   {
			   		niz["email_adresa"]=value.value;
			   }
			   else
			   {
			   		niz[value.name]=value.value;
			   }
			});
	    	console.log(niz);
			$.ajax({
	            method: "POST",
	            url: Settings.url_registracija,
	            data:niz,
	            success: function(data)
	            {
	                console.log(data);
	                if(data['Status']==="Korisnik nije uspesno registrovan!")
	                {
	                		alert("Korisnik vec postoji ili je doslo do problema na serveru pokusajte kasnije!");
	                }
	                else
	                {
	                		
	                		window.location.replace(Settings.url_pocetna);
	                }

	                
	            },
	            dataType: "json"
        	});

		});
    });



})(jQuery);