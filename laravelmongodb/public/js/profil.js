(function ($) {
    "use strict";


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(document).ready(function(){
    	$(".zakazivanjepranja").hide();
    	$(".zakazivanjeservisa").hide();
    	$("#servisnaIstorija").hide();
    	ucitajkorisnika();
    	listazakazivanja();
    	iscrtajvozila();

    });
    function listazakazivanja()
    {
    	$.ajax({
            method: "POST",
            url: Settings.url_listasvihzakazivanjejednogkorisnika,
            success: function(data)
            {
                console.log(data); 
                $(".listazakazanihusluga").empty();
                $.each(data,function(index,value){
                	var s="<tr><td>"+value["tip_rezervacije"]+"</td>";
                    s+="<td>"+value["zakazani_datum"]+"</td>";
                    s+="<td>"+value["registraciona_oznaka"]+"</td></tr>";
                    $(".listazakazanihusluga").append(s);
                });
            },
            dataType: "json"
    	});
    }
    function ucitajkorisnika()
    {
    	$.ajax({
            method: "POST",
            url: Settings.url_vratisveokorisniku,
            success: function(data)
            {
                console.log(data);
                $("#email").val(data["email_adresa"]);
                $("#lozinka").val(data["lozinka"]);
                $(".kontakt_telefon").val(data["kontakt_telefon"]);
            },
            dataType: "json"
    	});
    }

    $(".novovozilo").click(function(){

    	$(".novovozilo").hide();
    	var s="<tr class='dodavanjenovogvozilaimputi'><td><input type='text' name='registraciona_oznaka'  class='form-control podacizanovo' style='max-height: 40px;'></td>";
		s+="<td><input type='text' name='marka_vozila'  class='form-control  podacizanovo' style='max-height: 40px;'></td>";
		s+="<td><input type='text'  name='model_vozila'  class='form-control  podacizanovo' style='max-height: 40px;'></td>";
		s+="<td><input type='text'  name='godina_proizvodnje'  class='form-control  podacizanovo' style='max-height: 40px;'></td>";
		s+="<td><input type='text'  name='kilometraza'  class='form-control  podacizanovo' style='max-height: 40px;'></td>";
		s+="<td><select name='vrsta_vozila' class='form-control  podacizanovo' style='max-height: 40px;min-width: 110px;'>";
		s+="<option value='' disabled selected hidden>Izaberite</option>";
		s+="<option value='Kupe'>Kupe</option><option value='Limuzina'>Limuzina</option>";
		s+="<option value='Karavan'>Karavan</option><option value='Hecbek'>Hečbek</option>";
		s+="<option value='Kabriolet'>Kabriolet</option><option value='MiniVan'>MiniVan</option>";
		s+="<option value='Dzip'>Džip</option><option value='Pikap'>Pikap</option>";
		s+="<option value='Kombi'>Kombi</option></select></td>";
		s+="<td><input type='text'  name='vrsta_motora'  class='form-control  podacizanovo' style='max-height: 40px;'></td>";
		s+="<td><select name='vrsta_goriva' class='form-control  podacizanovo' style='max-height: 40px;min-width: 110px;'>";
		s+="<option value='' disabled selected hidden>Izaberite</option>";
		s+="<option value='Benzin'>Benzin</option><option value='Dizel'>Dizel</option>";
		s+="</select></td><td></td><td><input type='button' value='Dodaj' class='btn btn-primary btn-sm dodajvozilo'></td>";
		s+="</tr>";
        $(".listavozila").append(s);          
        $(".dodajvozilo").click(function(){

        		var ulazi=$(".podacizanovo");
        		var niz={};
        		var count=0;
        		$.each(ulazi,function(index,value){
        			if(value.value.length > 1)
        			{
        				count++;
        				niz[value.name]=value.value;
        			}
        			else
        			{
        				$.confirm({
							title: "Neuspesna kreiranje",
							content: "Neiste popunili polje "+value.name,
							buttons: {
								Uredu: function(){
									
								},
								
							}
						});
						return false;
        			}
        		});	     	   		
        		if(count === 8)
        		{
					console.log(niz);
					$.ajax({
				        method: "POST",
				        url: Settings.url_novovozilokorisnika,
				        data:niz,
				        success: function(data)
				        {
				            //console.log(data);
				            
				            if(data["Status"]==="Vozilo je uspesno dodato u garazu!")
				            {
				            	//$(".dodavanjenovogvozilaimputi").empty();
				            	iscrtajvozila();
				            	$(".novovozilo").show();
				            }
				        },
				        dataType: "json"
					});
        		}
        });
    });

    function iscrtajvozila()
    {
    	$(".listavozila").empty();
    	$.ajax({
	        method: "POST",
	        url: Settings.url_vozilajednogkorisnika,
	        success: function(data)
	        {
	            //console.log(data);
	            
	            if(data["Status"] === "Iscitavanje vozila korisnika nije moguce!")
	            {
	            	
	            }
	            else
	            {
	            	$.each(data,function(index,value){
	            			var s="<tr><td>"+value["registraciona_oznaka"]+"</td>";
							s+="<td>"+value["marka_vozila"]+"</td>";
							s+="<td>"+value["model_vozila"]+"</td>";
							s+="<td>"+value["godina_proizvodnje"]+"</td>";
							s+="<td>"+value["kilometraza"]+"</td>";
							s+="<td>"+value["vrsta_vozila"]+"</td>";
							s+="<td>"+value["vrsta_motora"]+"</td>";
							s+="<td>"+value["vrsta_goriva"]+"</td>";
							s+="<td><a href='#servisnaIstorija'><input type='button' value='Servisna istorija' class='btn btn-primary btn-sm'  id='S"+value['registraciona_oznaka']+"'></a></td>";
							s+="<td><input type='button' value='Ukloni' class='btn btn-primary btn-sm' id='"+value['registraciona_oznaka']+"'></td></tr>";
							$(".listavozila").append(s);

							$("#S"+value["registraciona_oznaka"]).click(function(){
								$('.registracioneselektovane').val(value["registraciona_oznaka"]);
								servisnaistorijaprkaz();
								
							});

							$("#"+value["registraciona_oznaka"]).click(function(){
								$.confirm({
									title: "Brisanje Vozila",
									content: "Da li ste sigurni da zelite obrisati vozilo sa registracionom oznakom: "+value["registraciona_oznaka"],
									buttons: {
										Da: function(){
											$.ajax({
									            method: "POST",
									            url: Settings.url_brisanjekorisnika,
									            data: {"registraciona_oznaka":value["registraciona_oznaka"]},
									            success: function(data)
									            {
									                iscrtajvozila();
									            },
									            dataType: "json"
									    	});
										},
										Ne: function(){

										},
										
									}
								});
							});
	            	});
	            }
	        },
	        dataType: "json"
		});
    }


    $(".zakazipranje").click(function(){
    	$(".zakazivanjepranja").show();
    	
    });

    $(".zakaziservis").click(function(){
    	$(".zakazivanjeservisa").show();
    });

    $(".zatvoriservisnuistoriju").click(function(){
    	$("#servisnaIstorija").hide();
    });

    $(".zatvorizakazivanjeservisa").click(function(){
    	$(".zakazivanjeservisa").hide();
    });

    $(".zatvorizakazivanjepranja").click(function(){
    	$(".zakazivanjepranja").hide();
    });


    $(".dugmezakazirezzaservisa").click(function(){
    	var niz={};
    	if($(".datumzazakazivanjeservisa").val().length>0)
    	{
    		if($(".vrstaradova").val().length>0)
    		{
    			if($(".opisproblemazakazivanje").val().length>0)
    			{
		    		niz["zakazani_datum"]=$(".datumzazakazivanjeservisa").val();
			    	niz["vrsta_radova"]=$(".vrstaradova").val();
			    	niz["registraciona_oznaka"]=$(".registracioneselektovane").val();
			    	niz["opis_problema"]=$(".opisproblemazakazivanje").val();
			    	$.ajax({
			            method: "POST",
			            url: Settings.url_novarezervacijazaservis,
			            data: niz,
			            success: function(data)
			            {
			            	
			                console.log(data);
			                if(data["Status"]==="Uspesno je zakazano servisiranje!")
			                {
			                	listazakazivanja();
								$(".datumzazakazivanjeservisa").val("");
			    				$(".vrstaradova").val("");
			    				$(".opisproblemazakazivanje").val("");
			                	$(".zakazivanjeservisa").hide();
			                }
			                else
			                {
			                	$.confirm({
										title: "Problem sa zakazivanjem",
										content: "Zakazivanje nije uspelo pokusajte neki drugi datum",
										buttons: {
											Uredu: function(){
												
											},
											
										}
									});
			                }
			               
			                
			            },
			            dataType: "json"
			    	});
			    }
			    else
			    {
			    	$.confirm({
						title: "Problem sa zakazivanjem",
						content: "Morate popuniti sva polja potrebna za zakazivanje",
						buttons: {
							Uredu: function(){
								
							},
							
						}
					});
			    }
	    	}
	    	else
	    	{
	    		$.confirm({
					title: "Problem sa zakazivanjem",
					content: "Morate popuniti sva polja potrebna za zakazivanje",
					buttons: {
						Uredu: function(){
							
						},
						
					}
				});
	    	}

    	}
    	else
    	{
			$.confirm({
				title: "Problem sa zakazivanjem",
				content: "Morate popuniti sva polja potrebna za zakazivanje",
				buttons: {
					Uredu: function(){
						
					},
					
				}
			});
    	}
    });



    $(".dugmezakazirezzapranje").click(function(){
    	var niz={};
    	console.log($(".zakzivanjevrstapranja").val());
    	if($(".datumzakazivanjapranja").val().length>0)
    	{
    		if($(".zakzivanjevrstapranja").val().length>0)
    		{
	    		niz["zakazani_datum"]=$(".datumzakazivanjapranja").val();
		    	niz["vrsta_pranja"]=$(".zakzivanjevrstapranja").val();
		    	niz["registraciona_oznaka"]=$(".registracioneselektovane").val();
		    	$.ajax({
		            method: "POST",
		            url: Settings.url_novarezervacijazapranje,
		            data: niz,
		            success: function(data)
		            {
		            	
		                console.log(data);
		                if(data["Status"]==="Uspesno zakazano pranje!")
		                {
		                	listazakazivanja();
							$(".datumzakazivanjapranja").val("");
		    				$(".zakzivanjevrstapranja").val("");
		                	$(".zakazivanjepranja").hide();
		                }
		                else
		                {
		                	$.confirm({
									title: "Problem sa zakazivanjem",
									content: "Zakazivanje nije uspelo pokusajte neki drugi datum",
									buttons: {
										Uredu: function(){
											
										},
										
									}
								});
		                }
		               
		                
		            },
		            dataType: "json"
		    	});
	    	}
	    	else
	    	{
	    		$.confirm({
					title: "Problem sa zakazivanjem",
					content: "Morate popuniti sva polja potrebna za zakazivanje",
					buttons: {
						Uredu: function(){
							
						},
						
					}
				});
	    	}

    	}
    	else
    	{
			$.confirm({
				title: "Problem sa zakazivanjem",
				content: "Morate popuniti sva polja potrebna za zakazivanje",
				buttons: {
					Uredu: function(){
						
					},
					
				}
			});
    	}
    });

    function servisnaistorijaprkaz()
    {
    	$("#servisnaIstorija").show();
    	$(".listauservisnojistoriji").empty();
    	$.ajax({
            method: "POST",
            url: Settings.url_servisnaistorijavozila,
            data: {"registraciona_oznaka":$(".registracioneselektovane").val()},
            success: function(data)
            {
            	
                console.log(data);
                $.each(data,function(index,value){
                	var s="<tr><td style='min-width: 120px;'>"+value["datum_servisiranja"]+"</td>";
					s+="<td>"+value["kilometraza_u_trenutku_servisiranja"]+"</td>";
					s+="<td style='width: auto;'>"+value["opis_servisiranja"]+"</td></tr>";
					$(".listauservisnojistoriji").append(s);
                });
                
            },
            dataType: "json"
    	});
    }

})(jQuery);