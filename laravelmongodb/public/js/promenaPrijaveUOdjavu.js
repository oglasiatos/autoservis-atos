(function($){

	$(function(){
		console.log("Promena prijave u odjavu.");
		proveriDaLiJeKorisnikPrijavljen();
	});

	function proveriDaLiJeKorisnikPrijavljen()
	{
		console.log("Provera da li je korisnik prijavljen");
		$.ajax({
			method: "POST",
			url: Settings.proveravanjeDaLiJeKorisnikPrijavljen_url,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Korisnik je prijavljen!")
				{
					$('.prijava').empty();
					var p = "<a href='profil.html' class='nav-link'>Profil</a>";
					$('.prijava').append(p);
				}
				else if(data['Status'] === "Serviser je prijavljen!")
				{
					$('.prijava').empty();
					var p = "<a href='serviser.html' class='nav-link'>Profil</a>";
					$('.prijava').append(p);
				}
				else if(data['Status'] === "Radnik u perionici je prijavljen!")
				{
					$('.prijava').empty();
					var p = "<a href='radnikUperionici.html' class='nav-link'>Profil</a>";
					$('.prijava').append(p);
				}
				if(data['Status'] != "Korisnik nije prijavljen!")
				{
					$('.registracija').empty();
					var s = "<a href='#' class='nav-link odjava'>Odjavi se</a>";
					$('.registracija').append(s);

					$('.odjava').click(function(){
						odjaviSe();
					});
				}
			},
			dataType: "json"
		});
	};

	function odjaviSe()
	{
		console.log("Odjavljivanje!");
		$.ajax({
			method: "POST",
			url: Settings.odjaviSe_url,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Korisnik je uspesno odjavljen!")
				{
					window.location = Settings.pocetnaStranica_url;
				}
				else
				{
					$.confirm({
						title: "Neuspela odjava",
						content: "Odjavljivanje nije uspelo. Pokusajte kasnije!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		})
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

})(jQuery);