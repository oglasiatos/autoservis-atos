(function($){

	$(function(){
		console.log("Radnik u servisu.");
		$('#servisnaIstorija').hide();
		izlistajSveZakazaneRezervacije();
	});

	$('.zatvoriServisnuIstoriju').click(function(){
		$('#servisnaIstorija').hide();
		//izlistajSveZakazaneRezervacije();
	});

	$('.zakaziNoviServis').click(function(){
		$('.zakaziNoviServis').hide();
		var s = "";
		s += "<tr><td><input type='email' name='email_adresa' id='cf-1' class='form-control rez' style='max-height: 40px;'></td>";
		s += "<td><input type='text' name='registraciona_oznaka'  class='form-control rez' style='max-height: 40px;'></td>";
		s += "<td><input name='zakazani_datum' type='text' id='cf-3'class='form-control rez datepicker px-2' style='max-height: 40px;'></td>";
		s += "<td><select name = 'vrsta_radova' class='form-control rez' style='max-height: 40px;min-width: 110px;'>";
		s += "<option value='' disabled selected hidden>Izaberite</option>";
		s += "<option value='Mehanicki Radovi'>Mehanički radovi</option>";
		s += "<option value='Elektro Radovi'>Elektro radovi</option>";
		s += "</select></td>";
		s += "<td><input type='text' name='opis_problema'  class='form-control rez' style='max-height: 40px;'></td>";
		s += "<td><button class=' float-right btn btn-primary btn-sm dodajNovuRezervaciju'>Dodaj</button></td>";
		s += "</tr>";
		$('.zakazaneRezervacije').append(s);
		$('.dodajNovuRezervaciju').click(function(){
			prikupljanjeInformacijaZaDodavanjeNoveRezervacije();
		});
	});

	function prikupljanjeInformacijaZaDodavanjeNoveRezervacije()
	{
		var input = $('.rez');
		var stringArray = {};
		var b = true;
		for(var i = 0; i < input.length; i++)
		{
			if($(input[i]).val() === "" || $(input[i]).val()===null)
			{
				b = false;
			}
			else
			{
				stringArray[$(input[i]).attr('name')] = $(input[i]).val();
			}
		}
		console.log(stringArray);
		console.log(b);
		if(b)
		{
			$.confirm({
				title: "Prikupljeni podaci",
				content: "Da li ste sigurni da zelite da obavite rezervaciju?",
				buttons: {
					Da: function(){
						dodajRezervaciju(stringArray);
					},
					Ne: function(){

					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Neophodni podaci",
				content: "Svi podaci u vezi sa rezervacijom moraju biti uneseni!",
				buttons: {
					OK: function(){

					},
				}
			});
		}
	};

	function dodajRezervaciju(stringArray)
	{
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodavanjeNoveRezervacije_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Uspesno je zakazano servisiranje!")
				{
					$.confirm({
						title: "Zakazivanje nove rezervacije",
						content: "Rezervacija je uspesno obavljena!",
						buttons: {
							OK: function(){
								izlistajSveZakazaneRezervacije();
							},
						}
					});
				}
				else
				{
					$.confirm({
						title: "Zakazivanje nove rezervacije",
						content: "Rezervacija nije uspesno obavljena. Probajte sa unosom drugog datuma!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});

	};

	function izlistajSveZakazaneRezervacije()
	{
		console.log("Izlistavanje svih zakazanih rezervacija u servisu!");
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeSvihZakazanihRezervacijaUServisu_url,
			success: function(data){
				console.log(data);
				$('.zakazaneRezervacije').empty();
				var s = "";
				$.each(data, function(i, l){
					s += "<tr><td>"+l['email_korisnika']+"</td>";
					s += "<td>"+l['registraciona_oznaka']+"</td>";
					s += "<td>"+l['zakazani_datum']+"</td>";
					s += "<td>"+l['vrsta_radova']+"</td>";
					s += "<td>"+l['opis_problema']+"</td>";
					s += "<td><a href='#servisnaIstorija'><input type='button' value='Servisiraj' class='float-right btn btn-primary btn-sm obaviServisiranje"+l['registraciona_oznaka']+"'></a></td>";
					s += "</tr>";

					$('.zakazaneRezervacije').append(s);
					
					$('.obaviServisiranje'+l['registraciona_oznaka']).click(function(){
						otvoriServisnuIstoriju(l['email_korisnika'], l['registraciona_oznaka']);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	function otvoriServisnuIstoriju(email, registracija)
	{
		$('#servisnaIstorija').show();
		$('.redoviNeki').empty();
		var s = "<div class='form-group col-md-3'><label for='cf-3'>Datum servisiranja:</label>";
		s += "<input type='text' name='datum_servisiranja' id='cf-3' placeholder='Odaberite datum' class='form-control datepicker px-2 azuriranje'>";
		s += "</div><div class='form-group col-md-3'><label for='cf-1'>Kilometraža:</label>";
		s += "<input name= 'kilometraza_u_trenutku_servisiranja' type='text' id='cf-1' class='form-control azuriranje'>";
		s += "</div><div class='form-group col-md-4'><label for='cf-1'>Opis kvara:</label>";
		s += "<textarea name='opis_servisiranja' id='cf-1' class='form-control azuriranje'></textarea>";
		s += "</div>";
		s += "<div class='col-md-2 mt-5'>";
		s += "<button class='btn btn-primary azuriranjeServisneIstorijeVozila"+registracija+"'>Dodavanje</button>";
		s += "</div>";

		$('.redoviNeki').append(s);

		s = "";

		console.log(email);
		console.log(registracija);

		$('.azuriranjeServisneIstorijeVozila'+registracija).click(function(){
						prikupljanjeInformacija(email, registracija);
					});
	};

	function prikupljanjeInformacija(email, registracija)
	{
		var input = $('.azuriranje');
		var stringArray = {};
		var b = true;
		stringArray['email_adresa'] = email;
		stringArray['registraciona_oznaka'] = registracija;
		for(var i = 0; i < input.length; i++)
		{
			if($(input[i]).val() === "")
			{
				b = false;
			}
			else
			{
				stringArray[$(input[i]).attr('name')] = $(input[i]).val();
			}
		}
		console.log(stringArray);
		if(b)
		{
			$.confirm({
				title: "Azuriranje servisne istorije",
				content: "Da li ste sigurni da zelite da izvrsite azuriranje servisne istorije ovog vozila?",
				buttons: {
					Da: function(){
						azurirajServisnuIstorijuVozila(stringArray);
					},
					Ne: function(){

					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Azuriranje servisne istorije",
				content: "Sve informacije u vezi sa servisnom istorijom moraju biti unete!",
				buttons: {
					OK: function() {

					},
				}
			});
		}
	};

	function azurirajServisnuIstorijuVozila(stringArray)
	{
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.azuriranjeServisneIstorijeVozila_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Uspesno je azurirana servisna istorija vozila!")
				{
					$.ajax({
						method: "POST",
						url: Settings.obrisiVozilo_url,
						data: stringArray,
						success: function(data){
							console.log(data);
							if(data['Status'] === "Uspesno obrisano vozilo iz liste zakazanih vozila za perionicu!")
							{
								$.confirm({
									title: "Brisanje vozila",
									content: "Uspesno dodata servisna istorija o vozilu!",
									buttons: {
										OK: function(){
											izlistajSveZakazaneRezervacije();
											$('#servisnaIstorija').hide();
										},
									}
								});
							}
							else
							{
								$.confirm({
									title: "Brisanje vozila",
									content: "Unos nije obavljen!",
									buttons: {
										OK: function(){

										},
									}
								});
							}
						},
						dataType: "json"
					});
				}
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

})(jQuery);