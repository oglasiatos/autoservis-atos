(function($){

	$(function(){
		console.log("Detaljnije");
		$('.deoZaRezervaciju').hide();
	});

	$('.iznajmiVozilo').click(function(){
		$('.deoZaRezervaciju').show();
	});

	$('.zakazivanjeIznajmljivanja').click(function(){
		prikupljanjeInformacijaZaIznajmljivanje();
	});

	function prikupljanjeInformacijaZaIznajmljivanje()
	{
		var input = $('.informacijeZaIznajmljivanje');
		var stringArray = {};
		stringArray['id'] = $('.zakazivanjeIznajmljivanja').attr('id');
		var b = true;
		for(var i = 0; i < input.length; i++)
		{
			if($(input[i]).attr('name')==="datum_od" || $(input[i]).attr('name') === "datum_do")
			{
				if($(input[i]).val()==="")
				{
					b=false;
				}
				else
				{
					stringArray[$(input[i]).attr('name')] = $(input[i]).val();
				}
			}
			else
			{
				stringArray[$(input[i]).attr('name')] = $(input[i]).is(":checked");
			}
		}
		console.log(stringArray);
		console.log(b);
		if(b)
		{
			console.log("Iznajmi vozilo!");
			$.ajax({
				method: "POST",
				url: Settings.iznajmiVozilo_url,
				data: stringArray,
				success: function(data){
					console.log(data);
					if(data['Status'] === "Uspesno obavljena rezervacija iznajmljivanja!")
					{
						$.confirm({
							title:"Rezervacija",
							content: "Rezervacija vozila je uspesno obavljena!",
							buttons: {
								OK: function(){
									location.reload();
								},
							}
						});
					}
					else
					{
						$.confirm({
							title: "Rezervacija",
							content: "Rezervacija vozila nije uspela. Pokusajte sa drugim datumima!",
							buttons: {
								OK: function(){
									
								},
							}
						});
					}
				},
				dataType: "json"
			});
		}
		else
		{
			$.confirm({
				title: "Nevalidno unesene informacije!",
				content: "Datumi iznajmljivanja i vracanja moraju biti uneti!",
				buttons: {
					OK: function(){

					},
				}
			});
		}
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });



})(jQuery);