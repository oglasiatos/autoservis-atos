(function($){

	$(function(){
		console.log("Cenovnik servisa!");
		izlistavanjeCenaRadova("Mehanicki Radovi");
		izlistavanjeCenaRadova("Elektro Radovi");
	});

	function izlistavanjeCenaRadova(kategorija)
	{
		console.log("Izlistavanje po kategoriji: "+kategorija);
		var stringArray = {};
		stringArray['kategorija'] = kategorija;
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeCenaRadova_url,
			data: stringArray,
			success: function(data){
				console.log(data);

				var s = "";
				$.each(data, function(i, l){

					s+="<tr><td>"+l['usluga']+"</td>";
					s+="<td>"+l['cena_usluge']+"</td></tr>";

				});

				if(kategorija==="Mehanicki Radovi")
				{
					$('.mehanickiRadovi').empty();
					$('.mehanickiRadovi').append(s);
				}
				else
				{
					$('.elektroRadovi').empty();
					$('.elektroRadovi').append(s);
				}
			},
			dataType: "json"
		});

	};





	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

})(jQuery);