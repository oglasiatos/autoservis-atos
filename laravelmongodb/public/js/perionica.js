(function($){

	$(function(){
		console.log("Perionica!");
		izlistavanjeCenovnikaPerionice();
	});

	function izlistavanjeCenovnikaPerionice()
	{
		console.log("Usluge perionice!");
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeCenovnikaPerionice_url,
			success: function(data){
				console.log(data);
				$('.cenovnikPerionice').empty();
				var s = "";
				$.each(data, function(i, l){
					s+="<tr><td>"+l['usluga']+"</td>";
					s+="<td>"+l['cena_usluge']+"</td></tr>";
				});
				$('.cenovnikPerionice').append(s);
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });


})(jQuery);