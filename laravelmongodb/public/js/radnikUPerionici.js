(function($){

	$(function(){
		console.log("Radnik u perionici");
		//$('#novaRezervacija').hide();
		izlistajSveRezervacijePranja();
	});

	function izlistajSveRezervacijePranja()
	{
		$('.zakazanaPranja').empty();
		var s = "";
		$.ajax({
			method: "POST",
			url: Settings.listanjeSvihZakazanihPranja_url,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<tr><td>"+l['email_korisnika']+"</td>";
					s += "<td>"+l['registraciona_oznaka']+"</td>";
					s += "<td>"+l['zakazani_datum']+"</td>";
					s += "<td>"+l['vrsta_pranja']+"</td>";
					s += "<td><input type='button' value='Operi' class='float-right btn btn-primary btn-sm pranjeKola"+l['id']+"'></td>";
					s += "</tr>";

					$('.zakazanaPranja').append(s);

					$('.pranjeKola'+l['id']).click(function(){
						$.confirm({
							title: "Pranje kola",
							content: "Da li ste sigurni da zelite da oznacite automobil kao opran?",
							buttons: {
								Da: function(){
									operiKola(l['id']);
								},
								Ne: function(){

								},
							}
						});
					});

					s = "";

				});
			},
			dataType: "json"
		});
	};

	function operiKola(id)
	{
		console.log(id);
		var stringArray = {};
		stringArray['id'] = id;
		$.ajax({
			method: "POST",
			url: Settings.operiKola_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Uspesno obrisano vozilo iz liste zakazanih vozila za perionicu!")
				{
					$.confirm({
						title: "Brisanje vozila",
						content: "Uspesno obavljeno oznacavanje automobila kao opranog!",
						buttons: {
							OK: function(){
								izlistajSveRezervacijePranja();
							},
						}
					});
				}
				else
				{
					$.confirm({
						title: "Brisanje vozila",
						content: "Aktivnost pranja nije uspela!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	$('.zakazivanjePranja').click(function(){
		$('.zakazivanjePranja').hide();
		console.log("CAO KALE");
		var s = "";
		s += "<tr id='novaRezervacija'><td><input type='email' name='email_korisnika' id=cf-1 class='form-control' style='max-height: 40px;'></td>";
		s += "<td><input type='text' name='registraciona_oznaka'  class='form-control' style='max-height: 40px;'></td>";
		s += "<td><input type='text' name='zakazani_datum' id='cf-3'class='form-control datepicker px-2' style='max-height: 40px;'></td>";
		s += "<td><select name='vrsta_pranja' class='form-control' style='max-height: 40px;min-width: 110px;'>";
		s += "<option value='' disabled selected hidden>Izaberite</option>";
		s += "<option value='Pranje spolja'>Pranje spolja</option>";
		s += "<option value='Pranje spolja toplom vodom'>Pranje spolja toplom vodom</option>";
		s += "<option value='Usisavanje'>Usisavanje</option>";
		s += "<option value='Dubinsko usisavanje'>Dubinsko usisavanje</option>";
		s += "<option value='Sjaj guma'>Sjaj guma</option>";
		s += "<option value='Detajling'>Detajling</option>";
		s += " <option value='Voskiranje'>Voskiranje</option>";
		s += "<option value='Polimer zastita'>Polimer zaštita</option>";
		s += "<option value='Keramicka zastita'>Keramička zaštita</option>";
		s += "<option value='Visoki sjaj'>Visoki sjaj</option>";
		s += "<option value='Ciscenje glinom'>Čišćenje glinom</option>";
		s += "</select></td>";
		s += "<td><button class=' float-right btn btn-primary btn-sm dodavanjeNoveRezervacije'>Dodaj</button></td>";
		s += "</tr>";
		$('.zakazanaPranja').append(s);

		$('.dodavanjeNoveRezervacije').click(function(){
			prikupljanjeInformacijaZaNovuRezervaciju();
		});
	});

	function prikupljanjeInformacijaZaNovuRezervaciju()
	{
		console.log("Prikupljanje informacija");
		var stringArray = {};
		var input = $('.form-control');
		var b = true;
		for(var i = 0; i < input.length; i++)
		{
			if($(input[i]).val()==="")
			{
				b=false;
			}
			else
			{
				stringArray[$(input[i]).attr('name')] = $(input[i]).val();
			}
		}
		console.log(stringArray);
		if(b)
		{
			dodajNovuRezervacijuZaPranje(stringArray);
		}
		else
		{
			$.confirm({
				title: "Informacije za rezervaciju pranja",
				content: "Sve informacije u vezi sa rezervacijom moraju biti unete!",
				buttons: {
					OK: function(){

					},
				}
			});
		}
	};

	function dodajNovuRezervacijuZaPranje(stringArray)
	{
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodajNovuRezervacijuZaPranje_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Uspesno zakazano pranje!")
				{
					$.confirm({
						title: "Zakazano pranje",
						content: "Zakazivanje je uspešno obavljeno!",
						buttons: {
							OK: function(){
								$('.zakazivanjePranja').show();
								izlistajSveRezervacijePranja();
							},
						}
					});
				}
				else
				{
					$.confirm({
						title: "Zakazano pranje",
						content: "Zakazivanje nije uspelo! Pokusajte sa nekim drugim datumom!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });




})(jQuery);