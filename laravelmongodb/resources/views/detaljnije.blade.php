<!doctype html>
<html lang="en">

  <head>
    <title>Iznajmljivanje vozila</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">

            <div class="col-3 ">
              <div class="site-logo">
                <a href="index.html">Autoservis Atos<span class="service-1-icon">
                <span class="flaticon-valet"></span>
              </span></a>
              </div>
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li><a href="index.html" class="nav-link">Početna stranica</a></li>
                  <li><a href="servis.html" class="nav-link">Servis</a></li>
                  <li class="active"><a href="iznajmljivanjeVozila.html" class="nav-link">Iznajmljivanje vozila</a></li>
                  <li><a href="perionica.html" class="nav-link">Perionica</a></li>
                  <li class = 'prijava'><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
                  <li class = 'registracija'><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>

    <div class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay innerpage" style="background-image: url('images/hero_2.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 text-center">
              <h1>Naša vozila</h1>
              <p>Ovde možete videti našu ponudu vozila za iznajmljivanje.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div  class="site-section pt-5 pb-0 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-11 offset-1">
            <div class="trip-form">
              <div class="row">
                <div class="item-1 offset-1">
                <a href="#"><img src="images/img_4.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h2><a href="#">{{$naziv_vozila}}</a></h2>
                  <div class="rent-price"><span>{{$cena_po_danu}}</span>€ po danu </div>
                  </div>
                  <br>
                  <div class="row">
                  <div class="col-lg-4">
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">{{$broj_vrata}}</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">{{$broj_sedista}}</span>
                    </li>
                    <li>
                      <span>Vrsta vozila</span>
                      <span class="spec">{{$vrsta_vozila}}</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">{{$vrsta_goriva}}</span>
                    </li>
                    <li>
                      <span>Zapremina</span>
                      <span class="spec">{{$zapremina}}ccm</span>
                    </li>
                  </ul>
                  </div>
                  <div class="col-lg-4">
                  <ul class="specs">
                    <li>
                      <span>Godište</span>
                      <span class="spec">{{$godina_proizvodnje}}</span>
                    </li>
                    <li>
                      <span>Prtljažnik</span>
                      <span class="spec">{{$kapacitet_prtljaznika}}</span>
                    </li>
                    <li>
                      <span>Menjač</span>
                      <span class="spec">{{$automatski_ili_rucni_menjac}}</span>
                    </li>
                    <li>
                      <span>Daljinsko zaklučavanje</span>
                      <span class="spec">{{$daljinsko_centralno_zakljucavanje}}</span>
                    </li>
                    <li>
                      <span>Broj airbag-ova</span>
                      <span class="spec">{{$broj_airbagova}}</span>
                    </li>
                  </ul>
                  </div>
                  <div class="col-lg-4">
                  <ul class="specs">
                    <li>
                      <span>Klima</span>
                      <span class="spec">{{$klima}}</span>
                    </li>
                    <li>
                      <span>Navigacija</span>
                      <span class="spec">{{$gps}}</span>
                    </li>
                    <li>
                      <span>Bluetooth</span>
                      <span class="spec">{{$bluetooth}}</span>
                    </li>
                    <li>
                      <span>Dečje sedište</span>
                      <span class="spec">{{$sediste_za_dete}}</span>
                    </li>
                    <li>
                      <span>Dodatna oprema</span>
                      <span class="spec">{{$audio_plejer_ili_multimedija}}</span>
                    </li>
                  </ul>
                  </div>
                </div>
                <br>
                  <div class="d-flex float-right">
                    <a href="#iznajmljivanje" class="btn btn-primary iznajmiVozilo">Iznajmi</a>
                  </div>
                </div>
               </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

   
    <div  id='iznajmljivanje' class="deoZaRezervaciju site-section pt-20 pb-5 bg-light offset-1">
      <div class="container">
        <div class="row">
          <div class="col-10 offset-1">
            
              <form class="trip-form">
                <div class="row align-items-center mb-4">
                  <div class="col-md-6">
                    <h2 class="m-2" style="text-decoration: underline;text-decoration-color: #007BFF;font-size: 24px;">
                       Rezervacija</h2>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="cf-3">Datum od:</label>
                    <input type="text" id="cf-3" name='datum_od' placeholder="Odaberite datum" class="form-control datepicker px-2 informacijeZaIznajmljivanje">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="cf-3">Datum do:</label>
                    <input type="text" id="cf-3" name='datum_do' placeholder="Odaberite datum" class="form-control datepicker px-2 informacijeZaIznajmljivanje">
                  </div>
                   <div class="form-group col-md-4 mt-1">
                    <label for="cf-1">Dodatne pogodnosti:</label><br>
                      <input type="checkbox" id='vozac' name = 'vozac' class='informacijeZaIznajmljivanje' value="Vozac">
                      <label for="cf-1">Vozač</label> &nbsp; &nbsp; &nbsp;
                      <input type="checkbox" id='dostava_vozila' name='dostava_vozila' class='informacijeZaIznajmljivanje' value="Dostava">
                      <label for="cf-1">Dostava vozila</label>
                   </div>
                   <div class="col-md-2 mt-5">
                    <input type="button" id= '{{$id}}' value="Zakazivanje" class="btn btn-primary zakazivanjeIznajmljivanja">
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>


    <footer class="site-footer">
      <div class="container">
        <div class="row offset-1">
          <div class="col-lg-3">
            <h2 class="footer-heading mb-4">Briga o automobilima</h2>
                <p>Ovde smo da Vam ponudimo sveobuhvatnu brigu o Vašem automobilu kao što je servis, iznajmljivanje i pranje automobila. </p>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row">
              <div class="col-lg-3">
                <h2 class="footer-heading mb-4">Linkovi</h2>
                <ul class="list-unstyled">
                  <li><a href="servis.html">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html">Iznajmljivanje</a></li>
                  <li><a href="perionica.html">Perionica</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="profil.html">Profil</a></li>
                  <li><a href="prijavljivanje.html">Prijavljivanje</a></li>
                  <li><a href="registrovanje.html">Registrovanje</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="index.html">Početna stranica</a></li>
                  <li><a href="oNama.html">O nama</a></li>
                </ul>
              </div>
             
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p>
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by AtosTeam<sup>&copy;</sup>
             </p> 
            </div>
          </div>

        </div>
      </div>
    </footer>

    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>

    <!--script src="js/iznajmljivanjevozila.js"></script-->
    <script src="js/detaljnije.js"></script>
    <script src="js/jquery-confirm.min.js"></script>
    <script type="text/javascript">
      var Settings = {
        iznajmiVozilo_url: "{{ url('/iznajmiVozilo') }}",
        proveravanjeDaLiJeKorisnikPrijavljen_url: "{{ url('/proveravanjeDaLiJeKorisnikPrijavljen') }}",
        odjaviSe_url: "{{ url('/odjaviSe') }}",
        pocetnaStranica_url: "{{ url('/index.html') }}"
      }
    </script>
    <script src="js/promenaPrijaveUOdjavu.js"></script>

  </body>

</html>

