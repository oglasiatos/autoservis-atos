<!doctype html>
<html lang="en">

  <head>
    <title>Iznajmljivanje vozila</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">

            <div class="col-3 ">
              <div class="site-logo">
                <a href="index.html">Autoservis Atos<span class="service-1-icon">
                <span class="flaticon-valet"></span>
              </span></a>
              </div>
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li><a href="index.html" class="nav-link">Početna stranica</a></li>
                  <li><a href="servis.html" class="nav-link">Servis</a></li>
                  <li class="active"><a href="iznajmljivanjeVozila.html" class="nav-link">Iznajmljivanje vozila</a></li>
                  <li><a href="perionica.html" class="nav-link">Perionica</a></li>
                  <li class = 'prijava'><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
                  <li class = 'registracija'><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>

    <div class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay innerpage" style="background-image: url('images/hero_2.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 text-center">
              <h1>Naša vozila</h1>
              <p>Ovde možete videti našu ponudu vozila za iznajmljivanje.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div  class="site-section pt-5 pb-0 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-10 offset-1">
            <div class="trip-form">
                <!--div class="row align-items-center mb-4">
                  <div class="col-md-6">
                    <h3 class="m-0">Unesite period iznajmljivanja vozila</h3>
                  </div>
                </div-->
                <div class="row">
                  <div class="form-group col-md-2">
                    <label for="cf-3">Datum od:</label>
                    <input type="text" id="cf-3" placeholder="Iznajmljivanje" name='datum_od' class="form-control datepicker px-2 filtriranje">
                  </div>
                  <div class="form-group col-md-2">
                    <label for="cf-4">Datum do:</label>
                    <input type="text" id="cf-4" placeholder="Vraćanje" name='datum_do' class="form-control datepicker px-2 filtriranje">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="cf-1">Naziv vozila:</label>
                    <input type="text" id="cf-1" name='naziv_vozila' placeholder="Model,marka.." class="form-control filtriranje">
                  </div>
                  <div class="form-group col-md-2">
                    <label for="cf-1">Vrsta vozila:</label>
                     <select name= 'vrsta_vozila' class="form-control filtriranje">
                      <option value="" disabled selected hidden>Izaberite</option>
                        <option value="Kupe">Kupe</option>
                        <option value="Limuzina">Limuzina</option>
                        <option value="Karavan">Karavan</option>
                        <option value="Hecbek">Hečbek</option>
                        <option value="Kabriolet">Kabriolet</option>
                        <option value="MiniVan">MiniVan</option>
                        <option value="Dzip">Džip</option>
                        <option value="Pikap">Pikap</option>
                        <option value="Kombi">Kombi</option>
                      </select>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="cf-1">Broj sedišta:</label>
                     <select name='broj_sedista' class="form-control filtriranje">
                      <option value="" disabled selected hidden></option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="7">7</option>
                      </select>
                  </div>
                   <div class="form-group col-md-2">
                    <label for="cf-1">Broj vrata:</label>
                     <select name='broj_vrata' class="form-control filtriranje">
                      <option value="" disabled selected hidden></option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="cf-1">Menjač:</label><br>
                    <input checked="checked" type="radio" class='filtriranje' name="automatski_ili_rucni_menjac" value="Manuelni menjac">Manuelni<br>
                    <input  type="radio" name="automatski_ili_rucni_menjac" class='filtriranje' value="Automatski menjac">Automatski<br>
                  </div>
                    <div class="col-md-2">
                    <label for="cf-1">Cena:</label><br>
                    <input type="button" class='filtriranjeCeneRastuca' value="↑" name="Rastuca" class="btn btn-default"> &nbsp;
                    <input type="button" class='filtriranjeCeneOpadajuca' value="↓" name="Opadajuca" class="btn btn-default">
                  </div>
                  <div class="col-md-6 pt-5">
                    <input type="button" class="btn btn-primary filtriranjeVozila" value="Pretraži">
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row listavozila">
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="item-1">
                <a href="#"><img src="images/img_1.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3>
                  
                  <div class="rent-price"><span>250</span>€ po danu</div>
                  </div>
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">4</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">5</span>
                    </li>
                    <li>
                      <span>Vrsta menjača</span>
                      <span class="spec">Automatski</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">Benzin</span>
                    </li>
                  </ul>
                  <div class="d-flex action justify-content-center">
                    <a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div>
                </div>
              </div>
          </div>

          <div class="col-lg-4 col-md-6 mb-4">
            <div class="item-1">
                <a href="#"><img src="images/img_4.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3>
                  <div class="rent-price"><span>250</span>€ po danu</div>
                  </div>
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">4</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">5</span>
                    </li>
                    <li>
                      <span>Vrsta menjača</span>
                      <span class="spec">Automatski</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">Benzin</span>
                    </li>
                  </ul>
                  <div class="d-flex action justify-content-center">
                    <a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div>
                </div>
              </div>
          </div>

        <div class="col-lg-4 col-md-6 mb-4">
            <div class="item-1">
                <a href="#"><img src="images/img_3.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3>
                  <div class="rent-price"><span>250</span>€ po danu</div>
                  </div>
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">4</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">5</span>
                    </li>
                    <li>
                      <span>Vrsta menjača</span>
                      <span class="spec">Automatski</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">Benzin</span>
                    </li>
                  </ul>
                  <div class="d-flex action justify-content-center">
                    <a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div>
                </div>
              </div>
          </div>

         <div class="col-lg-4 col-md-6 mb-4">
            <div class="item-1">
                <a href="#"><img src="images/img_4.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3>
                  <div class="rent-price"><span>250</span>€ po danu</div>
                  </div>
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">4</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">5</span>
                    </li>
                    <li>
                      <span>Vrsta menjača</span>
                      <span class="spec">Automatski</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">Benzin</span>
                    </li>
                  </ul>
                  <div class="d-flex action justify-content-center">
                    <a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div>
                </div>
              </div>
          </div>

       <div class="col-lg-4 col-md-6 mb-4">
            <div class="item-1">
                <a href="#"><img src="images/img_2.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3>
                  <div class="rent-price"><span>250</span>€ po danu</div>
                  </div>
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">4</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">5</span>
                    </li>
                    <li>
                      <span>Vrsta menjača</span>
                      <span class="spec">Automatski</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">Benzin</span>
                    </li>
                  </ul>
                  <div class="d-flex action justify-content-center">
                    <a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div>
                </div>
              </div>
          </div>

        <div class="col-lg-4 col-md-6 mb-4">
            <div class="item-1">
                <a href="#"><img src="images/img_1.jpg" alt="Image" class="img-fluid"></a>
                <div class="item-1-contents">
                  <div class="text-center">
                  <h3><a href="#">Range Rover S64 Coupe</a></h3>
                  <div class="rent-price"><span>250</span>€ po danu</div>
                  </div>
                  <ul class="specs">
                    <li>
                      <span>Broj vrata</span>
                      <span class="spec">4</span>
                    </li>
                    <li>
                      <span>Broj sedišta</span>
                      <span class="spec">5</span>
                    </li>
                    <li>
                      <span>Vrsta menjača</span>
                      <span class="spec">Automatski</span>
                    </li>
                    <li>
                      <span>Vrsta goriva</span>
                      <span class="spec">Benzin</span>
                    </li>
                  </ul>
                  <div class="d-flex action justify-content-center">
                    <a href="#iznajmljivanje" class="btn btn-primary">Detaljnije</a>
                  </div>
                </div>
              </div>
          </div>

          <div  id="iznajmljivanje"class="col-12 offset-5">
            <span class="p-3">1</span>
            <a href="#" class="p-3">2</a>
            <a href="#" class="p-3">3</a>
            <a href="#" class="p-3">4</a>
          </div>
        </div>
      </div>
    </div>
    

    <footer class="site-footer">
      <div class="container">
        <div class="row offset-1">
          <div class="col-lg-3">
            <h2 class="footer-heading mb-4">Briga o automobilima</h2>
                <p>Ovde smo da Vam ponudimo sveobuhvatnu brigu o Vašem automobilu kao što je servis, iznajmljivanje i pranje automobila. </p>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row">
              <div class="col-lg-3">
                <h2 class="footer-heading mb-4">Linkovi</h2>
                <ul class="list-unstyled">
                  <li><a href="servis.html">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html">Iznajmljivanje</a></li>
                  <li><a href="perionica.html">Perionica</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="profil.html">Profil</a></li>
                  <li><a href="prijavljivanje.html">Prijavljivanje</a></li>
                  <li><a href="registrovanje.html">Registrovanje</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="index.html">Početna stranica</a></li>
                  <li><a href="oNama.html">O nama</a></li>
                </ul>
              </div>
             
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p>
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by AtosTeam<sup>&copy;</sup>
             </p> 
            </div>
          </div>

        </div>
      </div>
    </footer>

    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>
      <script type="text/javascript">
      var Settings={
        url_vrativozilazaiznajmljivanje:"{{url('/vrativozilazaiznajmljivanje')}}",
        url_vratifiltriranavozilazaiznajmljivanje:"{{url('/index.html')}}",
        filtriranjeVozilaZaIznajmljivanje_url: "{{ url('/filtriranjeVozilaZaIznajmljivanje') }}",
        detaljniPrikazVozila_url: "{{ url('/detaljniPrikazVozila') }}",
        proveravanjeDaLiJeKorisnikPrijavljen_url: "{{ url('/proveravanjeDaLiJeKorisnikPrijavljen') }}",
        odjaviSe_url: "{{ url('/odjaviSe') }}",
        pocetnaStranica_url: "{{ url('/index.html') }}"
      }
    </script>

    <script src="js/iznajmljivanjevozila.js"></script>
    <script src="js/promenaPrijaveUOdjavu.js"></script>
    <script src="js/jquery-confirm.min.js"></script>

  </body>

</html>

