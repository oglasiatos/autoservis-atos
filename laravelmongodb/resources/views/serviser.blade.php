<!doctype html>
<html lang="en">

  <head>
    <title>Serviser</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">
            <div class="col-3 ">
              <div class="site-logo">
                <a href="index.html">Autoservis Atos<span class="service-1-icon">
                <span class="flaticon-valet"></span>
              </span></a>
              </div>
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                   <li><a href="index.html" class="nav-link">Početna stranica</a></li>
                  <li class=""><a href="servis.html" class="nav-link">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html" class="nav-link">Iznajmljivanje vozila</a></li>
                  <li><a href="perionica.html" class="nav-link">Perionica</a></li>
                  <li class = 'prijava'><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
                  <li class = 'registracija'><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>

 <div id = 'datumi' class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay" style="background-image: url('images/hero_1.jpg')">
        <div class="container">
          <div class="row  align-items-center">
            <div class="col-lg-11">
             <table id="tabela" class="table table-striped table-dark">
                  <thead>
                      <tr>
                          <th>E-mail korisnika</th>
                          <th>Registracija</th>
                          <th>Zakazani datum</th>
                          <th>Vrsta radova</th>
                          <th>Opis problema</th>
                          <th><input type="button" value="Zakazi servis" class="float-right btn btn-secondary btn-sm zakaziNoviServis"></th>
                      </tr>
                  </thead>
                  <tbody class='zakazaneRezervacije'>
                      <tr>
                          <td>jovana.kostic@elfak.rs</td>
                          <td>SU 231 AB</td>
                          <td>07/01/2020</td>
                          <td>Eletro radovi</td>
                          <td>Opis problema</td>
                          <td><a href="#servisnaIstorija"><input type="button" value="Servisiraj" class="float-right btn btn-primary btn-sm"></a></td>
                      </tr>
                      <!--tr>
                          <td><input type="email" name="email adresa" id=cf-1 class="form-control" style="max-height: 40px;"></td>
                          <td><input type="text" name="registracija"  class="form-control" style="max-height: 40px;"></td>
                          <td><input type="text" id="cf-3"class="form-control datepicker px-2" style="max-height: 40px;"></td>
                          <td><select class="form-control" style="max-height: 40px;min-width: 110px;">
                      <option value="" disabled selected hidden>Izaberite</option>
                        <option value="Mehanicki radovi">Mehanički radovi</option>
                        <option value="Elektro radovi">Elektro radovi</option>
                      </select></td>
                          <td><input type="text" name="opis problema"  class="form-control" style="max-height: 40px;"></td>
                          <td><button class=" float-right btn btn-primary btn-sm">Dodaj</button></td>
                      </tr-->    
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

        <div id="servisnaIstorija" class="site-section pt-5 pb-5 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-10 offset-1">
            
              <form class="trip-form" action='javascript:'>
                <div   class="row align-items-center mb-4">
                  <div class="col-md-6">
                    <h2 class="m-2" style="text-decoration: underline;text-decoration-color: #007BFF;font-size: 24px;">
                       Servisna istorija</h2>
                  </div>
                  <div class="col-md-6">
                    <a href="#tabela">
                    <input type="button" class="float-right btn btn-default zatvoriServisnuIstoriju" value="x"></a>
                  </div>
                </div>
                <div class="row redoviNeki">
                  <div class="form-group col-md-3">
                    <label for="cf-3">Datum servisiranja:</label>
                    <input type="text" name='datum_servisiranja' id="cf-3" placeholder="Odaberite datum" class="form-control datepicker px-2 azuriranje">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="cf-1">Kilometraža:</label>
                    <input name= 'kilometraza_u_trenutku_servisiranja' type="text" id="cf-1" class="form-control azuriranje">
                  </div>
                   <div class="form-group col-md-4">
                    <label for="cf-1">Opis kvara:</label>
                    <textarea name='opis_servisiranja' id="cf-1" class="form-control azuriranje"></textarea>
                  </div>
                  
                  <!--div class="col-md-2 mt-5">
                    <button class="btn btn-primary azuriranjeServisneIstorijeVozila">Dodavanje</button>
                  </div-->
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>

        <footer class="site-footer">
      <div class="container">
        <div class="row offset-1">
          <div class="col-lg-3">
            <h2 class="footer-heading mb-4">Briga o automobilima</h2>
                <p>Ovde smo da Vam ponudimo sveobuhvatnu brigu o Vašem automobilu kao što je servis, iznajmljivanje i pranje automobila. </p>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row">
              <div class="col-lg-3">
                <h2 class="footer-heading mb-4">Linkovi</h2>
                <ul class="list-unstyled">
                  <li><a href="servis.html">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html">Iznajmljivanje</a></li>
                  <li><a href="perionica.html">Perionica</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="profil.html">Profil</a></li>
                  <li><a href="prijavljivanje.html">Prijavljivanje</a></li>
                  <li><a href="registrovanje.html">Registrovanje</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="index.html">Početna stranica</a></li>
                  <li><a href="oNama.html">O nama</a></li>
                </ul>
              </div>
             
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p>
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by AtosTeam<sup>&copy;</sup>
             </p> 
            </div>
          </div>

        </div>
      </div>
    </footer>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/jquery-confirm.min.js"></script>
    <script src="js/aos.js"></script>
    <script type="text/javascript">
      var Settings = {
        izlistavanjeSvihZakazanihRezervacijaUServisu_url: "{{ url('/izlistavanjeSvihZakazanihRezervacijaUServisu') }}",
        dodavanjeNoveRezervacije_url: "{{ url('/dodavanjeNoveRezervacije') }}",
        azuriranjeServisneIstorijeVozila_url: "{{ url('/azuriranjeServisneIstorijeVozila') }}",
        obrisiVozilo_url: "{{ url('/obrisiVozilo') }}",
        proveravanjeDaLiJeKorisnikPrijavljen_url: "{{ url('/proveravanjeDaLiJeKorisnikPrijavljen') }}",
        odjaviSe_url: "{{ url('/odjaviSe') }}",
        pocetnaStranica_url: "{{ url('/index.html') }}"
      }
    </script>
    <script src="js/serviser.js"></script>

    <script src="js/main.js"></script>
    <script src="js/promenaPrijaveUOdjavu.js"></script>

  </body>

</html>

