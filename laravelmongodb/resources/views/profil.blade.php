<!doctype html>
<html lang="en">

  <head>
    <title>Profil</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="registracioneselektovane" class="registracioneselektovane">

    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">
            <div class="col-3 ">
              <div class="site-logo">
                <a href="index.html">Autoservis Atos<span class="service-1-icon">
                <span class="flaticon-valet"></span>
              </span></a>
              </div>
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                   <li><a href="index.html" class="nav-link">Početna stranica</a></li>
                  <li><a href="servis.html" class="nav-link">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html" class="nav-link">Iznajmljivanje vozila</a></li>
                  <li><a href="perionica.html" class="nav-link">Perionica</a></li>
                  <li class="active prijava"><a href="profil.html" class="nav-link">Profil</a></li>
                  <!--li class = ''><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li-->
                  <li class = 'registracija'><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>

    <div class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay" style="background-image: url('images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-5 offset-1">
              <div class="feature-car-rent-box-1">
                <h3>Profil</h3>
                    <div class="form-group">
                    <label for="cf-1">E-mail adresa</label>
                    <input disabled="false" type="email" name="email_adresa" id="email" class="form-control" >
                  </div>
                  <div class="form-group">
                    <label for="cf-1">Lozinka</label>
                    <input id="lozinka" type="password" name="lozinka" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="cf-3">Kontakt telefon</label>
                    <input type="text" id="cf-3" name="kontakt_telefon"  class="form-control kontakt_telefon">
                  </div>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="feature-car-rent-box-1">
                <table class="table table-striped">
                  <thead>
                  <tr>
                  <th>Rezervacija</th>
                  <th>Datum</th>
                  <th>Registracija</th>
                  </tr>
                  </thead>
                  <tbody class="listazakazanihusluga">
                  <tr>
                  <td>Servis</td>
                  <td>01-02-2020</td>
                  <td>PO 028 GG</td>
                  </tr>
                  <tr>
                  <td>Perionica</td>
                  <td>02-07-2020</td>
                  <td>PI 076 VM</td>
                  </tr>
                  <tr>
                  <td>Iznajmljivanje</td>
                  <td>03-05-2020</td>
                  <td>PI 058 TV</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="site-section pt-0 pb-0 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-12">
            
              <form id="garaza" class="trip-form">
                <div class="row align-items-center mb-4">
                  <div class="col-md-6">
                    <h2 style="text-decoration: underline;text-decoration-color: #007BFF;">
                Garaža</h2>
                  </div>
                </div>
                <div class="row">
                  <table class="table table-striped table-dark">
                  <thead>
                      <tr>
                          <th>Registracija</th>
                          <th>Marka</th>
                          <th>Model</th>
                          <th>Godište</th>
                          <th>Kilometraža</th>
                          <th>Tip</th>
                          <th>Motor</th>
                          <th>Gorivo</th>
                          <td></td>
                          <th><input type="button" value="Novo vozilo" class="float-right btn btn-secondary btn-sm novovozilo"></th>
                      </tr>
                  </thead>
                  <tbody class="listavozila">
                      <tr>
                          <td>SU 231 AB</td>
                          <td>Fiat</td>
                          <td>Punto</td>
                          <td>2003.</td>
                          <td>230000</td>
                          <td>Kupe</td>
                          <td>1.4 HDi</td>
                          <td>Dizel</td>
                          <td><a href="#servisnaIstorija"><input type="button" value="Servisna istorija" class="btn btn-primary btn-sm"></a></td>
                          <td><input type="button" value="Ukloni" class="btn btn-primary btn-sm"></td>
                      </tr>    
                      <tr>
                          <td>NS 962 DB</td>
                          <td>Volkswagen</td>
                          <td>Golf</td>
                          <td>2011.</td>
                          <td>180000</td>
                          <td>Hecbek</td>
                          <td>1.8 TDi</td>
                          <td>Benzin</td>
                          <td><input type="button" value="Servisna istorija" class="btn btn-primary btn-sm"></td>
                          <td><input type="button" value="Ukloni" class="btn btn-primary btn-sm"></td>
                      </tr>
                      <!--tr>
                          <td><input type="text" name="registracija"  class="form-control" style="max-height: 40px;"></td>
                          <td><input type="text" name="marka"  class="form-control" style="max-height: 40px;"></td>
                          <td><input type="text"  name="model"  class="form-control" style="max-height: 40px;"></td>
                          <td><input type="text"  name="godiste"  class="form-control" style="max-height: 40px;"></td>
                          <td><input type="text"  name="kilometraza"  class="form-control" style="max-height: 40px;"></td>
                          <td><select name="tip" class="form-control" style="max-height: 40px;min-width: 110px;">
                      <option value="" disabled selected hidden>Izaberite</option>
                        <option value="Kupe">Kupe</option>
                        <option value="Limuzina">Limuzina</option>
                        <option value="Karavan">Karavan</option>
                        <option value="Hecbek">Hečbek</option>
                        <option value="Kabriolet">Kabriolet</option>
                        <option value="MiniVan">MiniVan</option>
                        <option value="Dzip">Džip</option>
                        <option value="Pikap">Pikap</option>
                        <option value="Kombi">Kombi</option>
                      </select></td>
                          <td><select name="gorivo" class="form-control" style="max-height: 40px;min-width: 110px;">
                            <option value="" disabled selected hidden>Izaberite</option>
                              <option value="Benzin">Benzin</option>
                              <option value="Dizel">Dizel</option>
                            </select></td>
                          <td></td>
                          <td><input type="button" value="Dodaj" class="btn btn-primary btn-sm"></td>
                          <td></td>
                      </tr-->    
                  </tbody>
              </table>
              </div>
              <div class="row">
              <div id="servisnaIstorija" class="col-lg-12">
                <br>
                <a href="#garaza">
                <input type="button"class="float-right btn btn-default btn-sm zatvoriservisnuistoriju" value="x"></a>
                <h2 class="m-2" style="text-decoration: underline;text-decoration-color: #007BFF;font-size: 24px;">
                Servisna istorija</h2>

                <table class="table table-striped">
                  <thead>
                  <tr>
                  <th>Datum</th>
                  <th>Kilometraža</th>
                  <th>Opis kvara</th>
                  <th><a href="#zakazivanjeServisa"><input type="button" value="Zakazivanje servisa" class="btn btn-primary btn-sm zakaziservis"></a></th>
                  <th><a href="#zakazivanjePranja"><input type="button" value="Zakazivanje pranja" class="btn btn-primary btn-sm zakazipranje"></th>
                  </tr>
                  </thead>
                  <tbody class="listauservisnojistoriji">
                  <tr>
                  <td style="min-width: 120px;">01-02-2020</td>
                  <td>160000</td>
                  <td style="width: auto;">Opis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvara</td>
                  </tr>
                  <tr>
                  <td>20-05-2020</td>
                  <td>240000</td>
                  <td style="width: auto;">Opis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvara</td>
                  </tr>
                  <tr>
                  <td>12-06-2020</td>
                  <td>130000</td>
                  <td style="width: auto;">Opis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvaraOpis kvara Opis kvara Opis kvara Opis kvara</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>    
            </form>
            </div>
        </div>
      </div>
    </div>

    <div  class="site-section pt-5 pb-5 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-10 offset-1">
            
              <form class="trip-form zakazivanjeservisa">
                <div id="zakazivanjeServisa" class="row align-items-center mb-4">
                  <div class="col-md-6">
                    <h2 class="m-2" style="text-decoration: underline;text-decoration-color: #007BFF;font-size: 24px;">
                       Zakazivanje servisa</h2>
                     </div>
                      <div class="col-md-6"><a href="#servisnaIstorija">
                  <input type="button" class="float-right btn btn-default btn-sm zatvorizakazivanjeservisa" value="x"></a>
                </div>                
              </div>
                  
               
                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="cf-3">Datum:</label>
                    <input type="text" id="cf-3" placeholder="Odaberite datum" class="form-control datepicker px-2 datumzazakazivanjeservisa">
                  </div>
                  <div class="form-group col-md-3 ">
                    <label for="cf-1">Opis problema:</label>
                    <input type="text" id="cf-1" placeholder="Problem.." class="form-control opisproblemazakazivanje">
                  </div>
                   <div class="form-group col-md-3">
                      <label for="cf-1">Vrsta radova:</label>
                       <select class="form-control vrstaradova">
                        <option value="" disabled selected hidden>Tip radova</option>
                          <option value="Mehanicki">Mehanički radovi</option>
                          <option value="Elektro">Elektro radovi</option>
                        </select>
                  </div>
                  
                  <div class="col-md-2 mt-5">
                    <button class="btn btn-primary dugmezakazirezzaservisa">Zakazivanje</button>
                  </div>
                </div>
                 </div>
              </form>
            </div>
        </div>
      </div>
    </div>

        <div class="site-section pt-5 pb-5 bg-light">
      <div class="container">
        <div  class="row">
          <div class="col-10 offset-1">
            
              <form class="trip-form zakazivanjepranja">
                <div  id="zakazivanjePranja" class="row align-items-center mb-4">
                  <div class="col-md-6">
                    <h2 class="m-2" style="text-decoration: underline;text-decoration-color: #007BFF;font-size: 24px;">
                       Zakazivanje pranja</h2>
                     </div>
                    <div class="col-md-6"><a href="#servisnaIstorija">
                  <input type="button" class="float-right btn btn-default btn-sm zatvorizakazivanjepranja" value="x"></a>
                </div>                
              </div>
                <div class="row">
                  <div class="form-group col-md-3">
                    <label for="cf-3">Datum:</label>
                    <input type="text" id="cf-3" placeholder="Odaberite datum" class="form-control datepicker px-2 datumzakazivanjapranja">
                  </div>
                  
                   <div class="form-group col-md-4 ">
                      <label for="cf-1">Vrsta pranja:</label>
                       <select class="form-control zakzivanjevrstapranja">
                        <option value="" disabled selected hidden>Odaberite vrstu pranja</option>
                          <option value="Pranje spolja">Pranje spolja</option>
                          <option value="Pranje spolja toplom vodom">Pranje spolja toplom vodom</option>
                          <option value="Usisavanje">Usisavanje</option>
                          <option value="Dubinsko usisavanje">Dubinsko usisavanje</option>
                          <option value="Sjaj guma">Sjaj guma</option>
                          <option value="Detajling">Detajling</option>
                          <option value="Voskiranje">Voskiranje</option>
                          <option value="Polimer zastita">Polimer zaštita</option>
                          <option value="Keramicka zastita">Keramička zaštita</option>
                          <option value="Visoki sjaj">Visoki sjaj</option>
                          <option value="Ciscenje glinom">Čišćenje glinom</option>
                        </select>
                  </div>
                  
                  <div class="col-md-2 mt-5">
                    <button class="btn btn-primary dugmezakazirezzapranje">Zakazivanje</button>
                  </div>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>

  <footer class="site-footer">
      <div class="container">
        <div class="row offset-1">
          <div class="col-lg-3">
            <h2 class="footer-heading mb-4">Briga o automobilima</h2>
                <p>Ovde smo da Vam ponudimo sveobuhvatnu brigu o Vašem automobilu kao što je servis, iznajmljivanje i pranje automobila. </p>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row">
              <div class="col-lg-3">
                <h2 class="footer-heading mb-4">Linkovi</h2>
                <ul class="list-unstyled">
                  <li><a href="servis.html">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html">Iznajmljivanje</a></li>
                  <li><a href="perionica.html">Perionica</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="profil.html">Profil</a></li>
                  <li><a href="prijavljivanje.html">Prijavljivanje</a></li>
                  <li><a href="registrovanje.html">Registrovanje</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="index.html">Početna stranica</a></li>
                  <li><a href="oNama.html">O nama</a></li>
                </ul>
              </div>
             
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p>
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by AtosTeam<sup>&copy;</sup>
             </p> 
            </div>
          </div>

        </div>
      </div>
    </footer>


    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>
    <script src="js/main.js"></script>
    <script src="js/jquery-confirm.min.js"></script>
    <script type="text/javascript">
      var Settings={
        url_novovozilokorisnika:"{{url('/novovozilokorisnika')}}",
        url_vozilajednogkorisnika:"{{url('/vrativozilojednogkorisnika')}}",
        url_brisanjekorisnika: "{{ url('/brisanjevozilakorisnika') }}",
        url_vratisveokorisniku:"{{url('/vratisveokorisniku')}}",
        url_listasvihzakazivanjejednogkorisnika:"{{url('/listarezervacijajednogkorisnika')}}",
        url_servisnaistorijavozila: "{{url('/servisnaistorijajednogvozila')}}",
        url_novarezervacijazapranje: "{{url('/profilzakzaivanjepranja')}}",
        url_novarezervacijazaservis: "{{url('/profilzakazivanjeservisa')}}",
        proveravanjeDaLiJeKorisnikPrijavljen_url: "{{ url('/proveravanjeDaLiJeKorisnikPrijavljen') }}",
        odjaviSe_url: "{{ url('/odjaviSe') }}",
        pocetnaStranica_url: "{{ url('/index.html') }}"
      }
    </script>
    <script src="js/profil.js"></script>
    <script src="js/promenaPrijaveUOdjavu.js"></script>
    <script src="js/jquery-confirm.min.js"></script>

  </body>

</html>

