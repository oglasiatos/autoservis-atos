<!doctype html>
<html lang="en">

  <head>
    <title>Servis</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">

  </head>

  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>



      <header  class="site-navbar site-navbar-target" style="position: relative;background-color: black;"  role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">
            <div class="col-3 ">
              <div class="site-logo">
                <a href="index.html">Autoservis Atos<span class="service-1-icon">
                <span class="flaticon-valet"></span>
              </span></a>
              </div>
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                   <li><a href="index.html" class="nav-link">Početna stranica</a></li>
                  <li class="active"><a href="servis.html" class="nav-link">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html" class="nav-link">Iznajmljivanje vozila</a></li>
                  <li><a href="perionica.html" class="nav-link">Perionica</a></li>
                  <li class = 'prijava'><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
                  <li class = 'registracija'><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>



    <div class="ftco-blocks-cover-1" >
      <div class="ftco-cover-1 overlay" style="background-image: url('images/hero_1.jpg');overflow: scroll; overflow-x: hidden;">
         <div class="site-navbar site-navbar-target" style="position: relative;background-color: transparent;"  role="banner"></div>
        <div class="container">
          <div class="row" >
            <div class="col-lg-6">
              <table class=" table table-striped table-dark">
                  <thead>
                      <tr>
                          <th>Mehanički radovi</th>
                      </tr>
                  </thead>
                  <thead>
                      <tr>
                          <th class="col-lg-2">Usluge</th>
                          <th>Cene</th>
                      </tr>
                  </thead>
                  <tbody class = 'mehanickiRadovi'>
                      <tr>
                          <td>Mali servis</td>
                          <td>50e</td>
                      </tr>
                      <tr>
                          <td>Veliki servis</td>
                          <td>300e</td>
                      </tr>           
                  </tbody>
              </table>
              
            </div>
             <div class="col-lg-6 ">
              <table class="table table-striped table-dark">
                  <thead>
                      <tr>
                          <th>Elektro radovi</th>
                      </tr>
                  </thead>
                  <thead>
                      <tr>
                          <th class="col-lg-2">Usluge</th>
                          <th>Cene</th>
                      </tr>
                  </thead>
                  <tbody class="elektroRadovi">
                      <tr>
                          <td>Dijagnostika gresaka</td>
                          <td>50e</td>
                      </tr>
                      <tr>
                          <td>Zamena alternatora</td>
                          <td>300e</td>
                      </tr>           
                  </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        <footer class="site-footer">
      <div class="container">
        <div class="row offset-1">
          <div class="col-lg-3">
            <h2 class="footer-heading mb-4">Briga o automobilima</h2>
                <p>Ovde smo da Vam ponudimo sveobuhvatnu brigu o Vašem automobilu kao što je servis, iznajmljivanje i pranje automobila. </p>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row">
              <div class="col-lg-3">
                <h2 class="footer-heading mb-4">Linkovi</h2>
                <ul class="list-unstyled">
                  <li><a href="servis.html">Servis</a></li>
                  <li><a href="iznajmljivanjeVozila.html">Iznajmljivanje</a></li>
                  <li><a href="perionica.html">Perionica</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="profil.html">Profil</a></li>
                  <li><a href="prijavljivanje.html">Prijavljivanje</a></li>
                  <li><a href="registrovanje.html">Registrovanje</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <ul class="list-unstyled mt-5">
                  <li><a href="index.html">Početna stranica</a></li>
                  <li><a href="oNama.html">O nama</a></li>
                </ul>
              </div>
             
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p>
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by AtosTeam<sup>&copy;</sup>
             </p> 
            </div>
          </div>

        </div>
      </div>
    </footer>
    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/aos.js"></script>
    <script type="text/javascript">
      var Settings = {
        izlistavanjeCenaRadova_url: "{{ url('/izlistavanjeCenaRadova') }}",
        proveravanjeDaLiJeKorisnikPrijavljen_url: "{{ url('/proveravanjeDaLiJeKorisnikPrijavljen') }}",
        odjaviSe_url: "{{ url('/odjaviSe') }}",
        pocetnaStranica_url: "{{ url('/index.html') }}"
      }
    </script>
    <script src="js/servis.js"></script>

    <script src="js/main.js"></script>
    <script src="js/promenaPrijaveUOdjavu.js"></script>
    <script src="js/jquery-confirm.min.js"></script>

  </body>

</html>

