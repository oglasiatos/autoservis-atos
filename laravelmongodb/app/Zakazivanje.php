<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Zakazivanje extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'zakazivanje';
    protected $fillable = ['email_korisnika', 'vozilo', 'zakazani_datum', 'tip_rezervacije', 'vrsta_radova', 'vrsta_pranja', 'opis_problema'];
}
