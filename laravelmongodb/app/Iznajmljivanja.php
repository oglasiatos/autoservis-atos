<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Iznajmljivanja extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'iznajmljivanja';
    protected $fillable = ['email_korisnika',
							'vozilo_koje_se_iznajmljuje',
							'iznajmljivanje_od',
							'iznajmljivanje_do',
							'dostava_vozila',
							'vozac'];
}
