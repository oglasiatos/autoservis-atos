<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Korisnik;

class KorisnikController extends Controller
{
	public function validacijaPodatakaZaRegistraciju($kontaktTelefon, $emailAdresa)
	{
		//$korisnik = ;
		if(is_null(Korisnik::where('email_adresa', $emailAdresa)->first()) && is_null(Korisnik::where('kontakt_telefon', $kontaktTelefon)->first()))
		{
			return true;
		}
		return false;
	}

    public function registracijaNovogKorisnika(Request $request)
    {
    	$json = $_POST;
    	if($this->validacijaPodatakaZaRegistraciju($json['kontakt_telefon'], $json['email_adresa']))
    	{
    		$noviKorisnik = new Korisnik();
    		$noviKorisnik->email_adresa = $json['email_adresa'];
    		$noviKorisnik->lozinka = $json['lozinka'];
    		$noviKorisnik->kontakt_telefon = $json['kontakt_telefon'];
    		$noviKorisnik->tip_korisnika = "Korisnik";
    		$noviKorisnik->save(); 
            $request->session()->put('servis',$json['email_adresa']);
    		return response()->json(['Status' => "Korisnik je uspesno registrovan!"]);

    	}
    	return response()->json(['Status' => "Korisnik nije uspesno registrovan!"]);
    }

    public function prijavljivanjeKorisnika(Request $request)
    {
    	$json = $_POST;
    	if(is_null(Korisnik::where('email_adresa', $json['email_adresa'])->where('lozinka', $json['lozinka'])->first()))
    	{
    		return response()->json(['Status' => "Prijavljivanje korisnika nije uspelo!"]);
    	}
        $request->session()->put('servis',$json['email_adresa']);
    	$korisnik = Korisnik::where('email_adresa', $json['email_adresa'])->where('lozinka', $json['lozinka'])->first();
        if($korisnik->tip_korisnika === "Korisnik")
        {
            return response()->json(['Status' => "Prijavljivanje korisnika je uspelo!"]);
        }
        else 
        {
            if($korisnik->tip_radnika === "Serviser")
            {
                return response()->json(['Status' => "Prijavljivanje servisera je uspelo!"]);
            }
            else
            {
                return response()->json(['Status' => "Prijavljivanje radnika u perionici je uspelo!"]);
            }
        }
    }

    public function validacijaRegistracioneOznake($registracionaOznaka)
    {
        if(!is_null(Korisnik::first()))
        {
            $sviKorisnici = Korisnik::all();
            foreach($sviKorisnici as $value)
            {
                $svaVozilaKorisnika = $value->vozila;
                if(!is_null($svaVozilaKorisnika))
                {
                    foreach($svaVozilaKorisnika as $val)
                    {
                        if(isset($val['registraciona_oznaka']))
                        {
                            if($val['registraciona_oznaka'] === $registracionaOznaka)
                                return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public function dodajNovoVozilo(Request $request)
    {
        $json = $_POST;
        //return response()->json($this->validacijaRegistracioneOznake($json['registraciona_oznaka']));
        if($this->validacijaRegistracioneOznake($json['registraciona_oznaka']))
        {
            if($request->session()->has('servis'))
            {
                $email = $request->session()->get('servis');
                $korisnik = Korisnik::where('email_adresa', $email)->first();
                if($korisnik)
                {
                    $korisnik->push('vozila', [ 'registraciona_oznaka'=>$json['registraciona_oznaka'],
                                                'marka_vozila' => $json['marka_vozila'],
                                                'model_vozila' => $json['model_vozila'],
                                                'godina_proizvodnje' => $json['godina_proizvodnje'],
                                                'kilometraza' => $json['kilometraza'],
                                                'vrsta_vozila' => $json['vrsta_vozila'],
                                                'vrsta_motora' => $json['vrsta_motora'],
                                                'vrsta_goriva' => $json['vrsta_goriva']
                                            ]);
                    return response()->json(['Status' => "Vozilo je uspesno dodato u garazu!"]);
                }
            }
        }
        return response()->json(['Status' => "Vozilo nije uspesno dodato u garazu!"]);
    }

    public function obrisiVoziloIzGaraze(Request $request)
    {
        $json = $_POST;
        if($request->session()->has('servis'))
        {
            $email = $request->session()->get('servis');
            $korisnik = Korisnik::where('email_adresa', $email)->first();
           
            foreach ($korisnik->vozila as $value) {
                if($value['registraciona_oznaka']===$json['registraciona_oznaka'])
                {
                    $korisnik->pull("vozila",$value);
                    break;
                }
            }
            //return response()->json($korisnik);
            //$korisnik->unset('vozila', ['registraciona_oznaka', $json['registraciona_oznaka']]);
            return response()->json(['Status' => "Uspesno obrisano vozilo iz garaze!"]);
        }
        return response()->json(['Status' => "Neuspesno obrisano vozilo iz garaze!"]);
    }

    public function dodajServisnuIstorijuVozila(Request $request)
    {
        $json = $_POST;
        $korisnik = Korisnik::where('email_adresa', $json['email_adresa'])->first();
        if(isset($korisnik->vozila))
        {
            $pom = $korisnik->vozila;
            $i = 0;
            $b = false;
            foreach($pom as $value)
            {
                if(isset($value['registraciona_oznaka']))
                {
                    if($value['registraciona_oznaka'] === $json['registraciona_oznaka'])
                    {
                        $b = true;
                        break;
                    }
                }
                $i = $i + 1;
            }
            if($b)
            {
                if(isset($pom[$i]['servisna_istorija']))
                {
                    array_push($pom[$i]['servisna_istorija'], ['datum_servisiranja' => (new \DateTime())->format('m/d/Y'), 'kilometraza_u_trenutku_servisiranja' => $json['kilometraza_u_trenutku_servisiranja'], 'opis_servisiranja' => $json['opis_servisiranja']]);
                }
                else
                {
                    $array = array(0 => ['datum_servisiranja' => (new \DateTime())->format('m/d/Y'), 'kilometraza_u_trenutku_servisiranja' => $json['kilometraza_u_trenutku_servisiranja'], 'opis_servisiranja' => $json['opis_servisiranja']]);
                    $pom[$i]['servisna_istorija'] = $array;
                }
                $pom[$i]['kilometraza'] = $json['kilometraza_u_trenutku_servisiranja'];
                $korisnik->vozila = $pom; 
                $korisnik->save();   
                return response()->json(['Status' => "Uspesno je azurirana servisna istorija vozila!"]);
            }
            return response()->json(['Status' => "Azuriranje vozila nije uspelo!"]);
        }
        else
        {
            return response()->json(['Status' => "Vozilo nije pronadjeno!"]);
        }
    }

    public function vratiSvaVozilaJednogKorisnika(Request $request)
    {
        $json = $_POST;
        if($request->session()->has('servis'))
        {
            $email = $request->session()->get('servis');
            $korisnik = Korisnik::where('email_adresa', $email)->first();
            if(isset($korisnik->vozila))
            {
                return response()->json($korisnik->vozila);
            }
        }
        return response()->json(['Status' => "Iscitavanje vozila korisnika nije moguce!"]);
    }

    public function vratiServisnuIstorijuJednogVozila(Request $request)
    {
        $json = $_POST;
        if($request->session()->has('servis'))
        {
            $email = $request->session()->get('servis');
            $korisnik = Korisnik::where('email_adresa', $email)->first();
            if(isset($korisnik->vozila))
            {
                $pom = $korisnik->vozila;
                foreach($pom as $value)
                {
                    if(isset($value['registraciona_oznaka']))
                    {
                        if($value['registraciona_oznaka'] === $json['registraciona_oznaka'] && 
                            isset($value['servisna_istorija']))
                        {
                            return $value['servisna_istorija'];
                        }
                    }
                }
            }
        }
        return null;
    }

    public function vratiIdKorisnikaNaOsnovuEmailAdrese(Request $request)
    {
        $json = $_POST;
        $id = Korisnik::where('email_adresa', $json['email_adresa'])->first();
        if(is_null($id))
            return response()->json(['Status' => "Korisnik ne postoji!"]);
        return response()->json(['Status' => $id]);
    }

    public function vratiSveInformacijeOKorisnikuPremaEmailAdresi(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        if($request->session()->has('servis'))
        {
            $korisnik = Korisnik::where('email_adresa', $request->session()->get('servis'))->first();
            $slanje['id'] = $korisnik['_id'];
            $slanje['email_adresa'] = $korisnik['email_adresa'];
            $slanje['lozinka'] = $korisnik['lozinka'];
            $slanje['kontakt_telefon'] = $korisnik['kontakt_telefon'];
            $slanje['tip_korisnika'] = $korisnik['tip_korisnika'];
            if($korisnik['tip_korisnika'] === "Radnik")
            {
                $slanje['tip_radnika'] = $korisnik['tip_radnika'];
            }
        }
        return response()->json($slanje);
    }

    public function proveriDaLiVoziloPostojiUBazi(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $b = false;
        if(!is_null(Korisnik::where('email_adresa', $json['email_adresa'])->first()))
        {
            $korisik = Korisnik::where('email_adresa', $json['email_adresa'])->first();
            if(isset($korisnik->vozila))
            {
                $i = 0;
                foreach($korisnik->vozila as $value)
                {
                    if(isset($value['registraciona_oznaka']))
                    {
                        if($value['registraciona_oznaka'] === $json['registraciona_oznaka'])
                        {
                            $b = true;
                            break;
                        }
                    }
                }
            }
        }
        if($b)
        {
            return response()->json(['Status' => "Vozilo postoji u bazi podataka!"]);
        }
        else
        {
            return response()->json(['Status' => "Vozilo ne postoji u bazi podataka!"]);
        }
    }

}
