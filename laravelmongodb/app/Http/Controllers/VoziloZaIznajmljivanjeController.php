<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VoziloZaIznajmljivanje;


class VoziloZaIznajmljivanjeController extends Controller
{
    public function vratiListuSvihVozila(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	if(!is_null(VoziloZaIznajmljivanje::first()))
    	{
    		$listaVozila = VoziloZaIznajmljivanje::all();
    		$i = 0;
    		foreach($listaVozila as $value)
    		{
    			$slanje[$i]['id'] = $value['_id'];
    			$slanje[$i]['naziv_vozila'] = $value['naziv_vozila'];
    			$slanje[$i]['broj_vrata'] = $value['broj_vrata'];
    			$slanje[$i]['broj_sedista'] = $value['broj_sedista'];
    			$slanje[$i]['zapremina'] = $value['zapremina'];
    			$slanje[$i]['automatski_ili_rucni_menjac'] = $value['automatski_ili_rucni_menjac'];
    			$slanje[$i]['cena_po_danu'] = $value['cena_po_danu'];
    			$slanje[$i]['kapacitet_prtljaznika'] = $value['kapacitet_prtljaznika'];
    			$slanje[$i]['vrsta_goriva'] = $value['vrsta_goriva'];
    			$slanje[$i]['sediste_za_dete'] = $value['sediste_za_dete'];
    			$slanje[$i]['vrsta_vozila'] = $value['vrsta_vozila'];
    			$slanje[$i]['gps'] = $value['gps'];
    			$slanje[$i]['klima'] = $value['klima'];
    			$slanje[$i]['audio_plejer_ili_multimedija'] = $value['audio_plejer_ili_multimedija'];
    			$slanje[$i]['bluetooth'] = $value['bluetooth'];
    			$slanje[$i]['broj_airbagova'] = $value['broj_airbagova'];
    			$slanje[$i]['daljinsko_centralno_zakljucavanje'] = $value['daljinsko_centralno_zakljucavanje'];
    			//$slanje[$i]['minibar'] = $value['minibar'];
    			$slanje[$i]['godina_proizvodnje'] = $value['godina_proizvodnje'];
    			$i = $i + 1;
    		}
    	}
    	return response()->json($slanje);
    }

    public function vratiVoziloZaIznajmljivanje(Request $request, $idVozila)
    {
    	$json = $_POST;
    	$slanje = null;
    	if(!is_null(VoziloZaIznajmljivanje::find($idVozila)))
    	{
    		$vozilo = VoziloZaIznajmljivanje::find($idVozila);
    		$slanje['id'] = $vozilo['_id'];
    		$slanje['naziv_vozila'] = $vozilo['naziv_vozila'];
    		$slanje['broj_sedista'] = $vozilo['broj_sedista'];
    		$slanje['broj_vrata'] = $vozilo['broj_vrata'];
    		$slanje['zapremina'] = $vozilo['zapremina'];
    		$slanje['automatski_ili_rucni_menjac'] = $vozilo['automatski_ili_rucni_menjac'];
    		$slanje['cena_po_danu'] = $vozilo['cena_po_danu'];
    		$slanje['kapacitet_prtljaznika'] = $vozilo['kapacitet_prtljaznika'];
    		$slanje['vrsta_goriva'] = $vozilo['vrsta_goriva'];
    		if($vozilo['sediste_za_dete']===true)
            {
                $slanje['sediste_za_dete'] = "Da";
            }
            else
            {
                $slanje['sediste_za_dete'] = "Ne";
            }
    		$slanje['vrsta_vozila'] = $vozilo['vrsta_vozila'];
    		if($vozilo['gps'] === true)
            {
                $slanje['gps'] = "Da";
            }
            else
            {
                $slanje['gps'] = "Ne";
            }
    		if($vozilo['klima'] === true)
            {
                $slanje['klima'] = "Da";
            }
            else
            {
                $slanje['klima'] = "Ne";
            }
    		$slanje['audio_plejer_ili_multimedija'] = $vozilo['audio_plejer_ili_multimedija'];
    		if($vozilo['bluetooth'] === true)
            {
                $slanje['bluetooth'] = "Da";
            }
            else
            {
                $slanje['bluetooth'] = "Ne";
            }
    		$slanje['broj_airbagova'] = $vozilo['broj_airbagova'];
    		if($vozilo['daljinsko_centralno_zakljucavanje'] === true)
            {
                $slanje['daljinsko_centralno_zakljucavanje'] = "Da";
            }
            else
            {
                $slanje['daljinsko_centralno_zakljucavanje'] = "Ne";
            }
    		//$slanje['minibar'] = $vozilo['minibar'];
    		$slanje['godina_proizvodnje'] = $vozilo['godina_proizvodnje'];
    	}
    	return view('detaljnije', $slanje);
    }

    public function proveriPoklapanjeDatuma($id, $datumOd, $datumDo)
    {
        $vozilo = VoziloZaIznajmljivanje::find($id);
        $listaRezervacijaVozila = $vozilo->lista_rezervacija;
        foreach($listaRezervacijaVozila as $value)
        {
            $rezervacijaOd = (new \DateTime($value['iznajmljeno_od']));
            $rezervacijaDo = (new \DateTime($value['iznajmljeno_do']));
            if($datumDo < $rezervacijaOd || $datumOd > $rezervacijaDo)
            {
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    public function proveriValidnostDatuma($id, $datum_od, $datum_do)
    {
        $datumOd = (new \DateTime($datum_od));
        $datumDo = (new \DateTime($datum_do));
        $danasnjiDatum = (new \DateTime("now"));
        if($datumOd <= $datumDo && $datumOd > $danasnjiDatum)
            return $this->proveriPoklapanjeDatuma($id, $datumOd, $datumDo);
        return false;
    }

    public function dodajRezervacijuZaIznajmljivanje(Request $request)
    {
        $json = $_POST;
        if($request->session()->has('servis'))
        {
            $email = $request->session()->get('servis');
            $vozilo = VoziloZaIznajmljivanje::find($json['id']);
            if(!is_null($vozilo))
            {
                if($this->proveriValidnostDatuma($json['id'], $json['datum_od'], $json['datum_do']))
                {
                    $vozilo->push('lista_rezervacija', ['email_korisnika' => $email,
                                                        'iznajmljeno_od' => $json['datum_od'],
                                                        'iznajmljeno_do' => $json['datum_do'],
                                                        'vozac' => $json['vozac'],
                                                        'dostava_vozila' => $json['dostava_vozila']
                                                    ]);

                    return response()->json(['Status' => "Uspesno obavljena rezervacija iznajmljivanja!"]);
                }
            }
        }
        return response()->json(['Status' => "Neuspesno obavljena rezervacija iznajmljivanja!"]);
    }

    public function izvrsiFiltriranje($cena, $vozila)
    {
        if($cena === "Rastuca")
        {
            $vozila = $vozila->orderBy('cena_po_danu', 'asc');
        }
        else
        {
            $vozila = $vozila->orderBy('cena_po_danu', 'desc');
        }
        return $vozila;
    }

    public function filtrirajKadNemaJednogDatuma($vozila, $datum)
    {
        $slanje = null;
        $i = 0;
        foreach($vozila as $value)
        {
            $b = true;
            if(isset($value->lista_rezervacija))
            {
                foreach($value->lista_rezervacija as $val)
                {
                    if(isset($val['iznajmljeno_od']) && isset($val['iznajmljeno_do']))
                    {
                        $iznajmljenoOd = (new \DateTime($val['iznajmljeno_od']));
                        $iznajmljenoDo = (new \DateTime($val['iznajmljeno_do']));
                        $d = (new \DateTime($datum));
                        if($d < $iznajmljenoDo && $d > $iznajmljenoOd)
                        {
                            $b = false;
                            break;
                        }
                    }
                }
            }
            if($b)
            {
                $slanje[$i]['id'] = $value['_id'];
                $slanje[$i]['naziv_vozila'] = $value['naziv_vozila'];
                $slanje[$i]['broj_sedista'] = $value['broj_sedista'];
                $slanje[$i]['broj_vrata'] = $value['broj_vrata'];
                $slanje[$i]['zapremina'] = $value['zapremina'];
                $slanje[$i]['automatski_ili_rucni_menjac'] = $value['automatski_ili_rucni_menjac'];
                $slanje[$i]['cena_po_danu'] = $value['cena_po_danu'];
                $slanje[$i]['kapacitet_prtljaznika'] = $value['kapacitet_prtljaznika'];
                $slanje[$i]['vrsta_goriva'] = $value['vrsta_goriva'];
                $slanje[$i]['sediste_za_dete'] = $value['sediste_za_dete'];
                $slanje[$i]['vrsta_vozila'] = $value['vrsta_vozila'];
                $slanje[$i]['gps'] = $value['gps'];
                $slanje[$i]['klima'] = $value['klima'];
                $slanje[$i]['audio_plejer_ili_multimedija'] = $value['audio_plejer_ili_multimedija'];
                $slanje[$i]['bluetooth'] = $value['bluetooth'];
                $slanje[$i]['broj_airbagova'] = $value['broj_airbagova'];
                $slanje[$i]['daljinsko_centralno_zakljucavanje'] = $value['daljinsko_centralno_zakljucavanje'];
                    //$slanje[$i]['minibar'] = $value['minibar'];
                $slanje[$i]['godina_proizvodnje'] = $value['godina_proizvodnje'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }

    public function filtrirajZaDatume($vozila, $datum_od, $datum_do)
    {
        $slanje = null;
        $i = 0;
        if($datum_od === "")
        {
            return $this->filtrirajKadNemaJednogDatuma($vozila, $datum_do);
        }
        else if($datum_do === "")
        {
            return $this->filtrirajKadNemaJednogDatuma($vozila, $datum_od);
        }
        else
        {
            foreach($vozila as $value)
            {
                $b = true;
                if(isset($value->lista_rezervacija))
                {
                    foreach($value->lista_rezervacija as $val)
                    {
                        if(isset($val['iznajmljeno_od']) && isset($val['iznajmljeno_do']))
                        {
                            $iznajmljenoOd = (new \DateTime($val['iznajmljeno_od']));
                            $iznajmljenoDo = (new \DateTime($val['iznajmljeno_do']));
                            $datumDo = (new \DateTime($datum_do));
                            $datumOd = (new \DateTime($datum_od));
                            if(($datumOd > $iznajmljenoDo) || ($datumDo < $iznajmljenoOd))
                            {
                            }
                            else
                            {
                                $b = false;
                                break;
                            }
                        }
                    }
                }
                if($b)
                {
                    $slanje[$i]['id'] = $value['_id'];
                    $slanje[$i]['naziv_vozila'] = $value['naziv_vozila'];
                    $slanje[$i]['broj_sedista'] = $value['broj_sedista'];
                    $slanje[$i]['broj_vrata'] = $value['broj_vrata'];
                    $slanje[$i]['zapremina'] = $value['zapremina'];
                    $slanje[$i]['automatski_ili_rucni_menjac'] = $value['automatski_ili_rucni_menjac'];
                    $slanje[$i]['cena_po_danu'] = $value['cena_po_danu'];
                    $slanje[$i]['kapacitet_prtljaznika'] = $value['kapacitet_prtljaznika'];
                    $slanje[$i]['vrsta_goriva'] = $value['vrsta_goriva'];
                    $slanje[$i]['sediste_za_dete'] = $value['sediste_za_dete'];
                    $slanje[$i]['vrsta_vozila'] = $value['vrsta_vozila'];
                    $slanje[$i]['gps'] = $value['gps'];
                    $slanje[$i]['klima'] = $value['klima'];
                    $slanje[$i]['audio_plejer_ili_multimedija'] = $value['audio_plejer_ili_multimedija'];
                    $slanje[$i]['bluetooth'] = $value['bluetooth'];
                    $slanje[$i]['broj_airbagova'] = $value['broj_airbagova'];
                    $slanje[$i]['daljinsko_centralno_zakljucavanje'] = $value['daljinsko_centralno_zakljucavanje'];
                    //$slanje[$i]['minibar'] = $value['minibar'];
                    $slanje[$i]['godina_proizvodnje'] = $value['godina_proizvodnje'];
                    $i = $i + 1;
                }
            }
        }
        return response()->json($slanje);
    }

    public function filtriranjeVozilaZaIznajmljivanje(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        $vozila = VoziloZaIznajmljivanje::where('automatski_ili_rucni_menjac', $json['automatski_ili_rucni_menjac']);
        if($json['naziv_vozila'] != "")
        {
            $vozila = $vozila->where('naziv_vozila', 'like', '%'.$json['naziv_vozila']."%");
        }
        if($json['broj_sedista'] != "")
        {
            $vozila = $vozila->where('broj_sedista', intval($json['broj_sedista']));
        }
        if($json['broj_vrata'] != "")
        {
            $vozila = $vozila->where('broj_vrata', intval($json['broj_vrata']));
        }
        if($json['vrsta_vozila'] != "")
        {
            $vozila = $vozila->where('vrsta_vozila', $json['vrsta_vozila']);
        }
        $vozila = $this->izvrsiFiltriranje($json['cena'], $vozila);
        $vozila = $vozila->get();
        if(isset($vozila[0]))
        {
            if($json['datum_od'] === "" && $json['datum_do']==="")
            {
                foreach($vozila as $value)
                {
                    $slanje[$i]['id'] = $value['_id'];
                    $slanje[$i]['naziv_vozila'] = $value['naziv_vozila'];
                    $slanje[$i]['broj_sedista'] = $value['broj_sedista'];
                    $slanje[$i]['broj_vrata'] = $value['broj_vrata'];
                    $slanje[$i]['zapremina'] = $value['zapremina'];
                    $slanje[$i]['automatski_ili_rucni_menjac'] = $value['automatski_ili_rucni_menjac'];
                    $slanje[$i]['cena_po_danu'] = $value['cena_po_danu'];
                    $slanje[$i]['kapacitet_prtljaznika'] = $value['kapacitet_prtljaznika'];
                    $slanje[$i]['vrsta_goriva'] = $value['vrsta_goriva'];
                    $slanje[$i]['sediste_za_dete'] = $value['sediste_za_dete'];
                    $slanje[$i]['vrsta_vozila'] = $value['vrsta_vozila'];
                    $slanje[$i]['gps'] = $value['gps'];
                    $slanje[$i]['klima'] = $value['klima'];
                    $slanje[$i]['audio_plejer_ili_multimedija'] = $value['audio_plejer_ili_multimedija'];
                    $slanje[$i]['bluetooth'] = $value['bluetooth'];
                    $slanje[$i]['broj_airbagova'] = $value['broj_airbagova'];
                    $slanje[$i]['daljinsko_centralno_zakljucavanje'] = $value['daljinsko_centralno_zakljucavanje'];
                    //$slanje[$i]['minibar'] = $value['minibar'];
                    $slanje[$i]['godina_proizvodnje'] = $value['godina_proizvodnje'];
                    $i = $i + 1;
                }
            }
            else
            {
                return $this->filtrirajZaDatume($vozila, $json['datum_od'], $json['datum_do']);
            }
        }
        return response()->json($slanje);
    }

    
}
