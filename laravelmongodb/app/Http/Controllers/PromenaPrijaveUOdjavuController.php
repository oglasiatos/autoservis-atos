<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Korisnik;

class PromenaPrijaveUOdjavuController extends Controller
{
    public function proveravanjeDaLiJeKorisnikPrijavljen(Request $request)
    {
    	$json = $_POST;
    	if($request->session()->has('servis'))
    	{
    		$korisnik = Korisnik::where('email_adresa', $request->session()->get('servis'))->first();
            if($korisnik->tip_korisnika === "Korisnik")
            {
                return response()->json(['Status' => "Korisnik je prijavljen!"]);
            }
            else
            {
                if($korisnik->tip_radnika === "Serviser")
                {
                    return response()->json(['Status' => "Serviser je prijavljen!"]);
                }
                else
                {
                    return response()->json(['Status' => "Radnik u perionici je prijavljen!"]);
                }
            }
    	}
    	return response()->json(['Status' => "Korisnik nije prijavljen!"]);
    }

    public function odjaviSe(Request $request)
    {
    	$json = $_POST;
    	if($request->session()->has('servis'))
    	{
    		$request->session()->forget('servis');
    		return response()->json(['Status' => "Korisnik je uspesno odjavljen!"]);
    	}
    	return response()->json(['Status' => "Korisnik nije odjavljen!"]);
    }
}
