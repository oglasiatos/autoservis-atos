<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cenovnik;

class CenovnikController extends Controller
{
    public function vratiSveUslugeZaPranje(Request $request)
    {
    	$json = $_POST;
    	$uslugePranja = Cenovnik::where('pranje_ili_servis', 'Pranje')->get();
    	$slanje = null;
    	$i = 0;
    	foreach($uslugePranja as $value)
    	{
    		$slanje[$i]['usluga'] = $value['usluga'];
    		$slanje[$i]['cena_usluge'] = $value['cena_usluge'];
    		$i = $i + 1;
    	}
    	return response()->json($slanje);
    }

    public function vratiSveUslugeZaServis(Request $request)
    {
    	$json = $_POST;
    	$uslugeServisiranja = Cenovnik::where('pranje_ili_servis', 'Servis')->get();
    	$slanje = null;
    	$i = 0;
    	foreach($uslugeServisiranja as $value)
    	{
    		$slanje[$i]['usluga'] = $value['usluga'];
    		$slanje[$i]['cena_usluge'] = $value['cena_usluge'];
    		$i = $i + 1;
    	}
    	return response()->json($slanje);
    }

    public function vratiUslugeZaServisPremaOdabranojKategoriji(Request $request)
    {
    	$json = $_POST;
    	$uslugeServisiranja = Cenovnik::where('pranje_ili_servis', 'Servis')->where('kategorija', $json['kategorija'])->get();
    	$slanje = null;
    	$i = 0;
    	foreach($uslugeServisiranja as $value)
    	{
    		$slanje[$i]['usluga'] = $value['usluga'];
    		$slanje[$i]['cena_usluge'] = $value['cena_usluge'];
    		$i = $i + 1;
    	}
    	return response()->json($slanje);
    }
}
