<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VoziloZaIznajmljivanje;
use App\Korisnik;

class TestController extends Controller
{
	

    public function testFunkcija(Request $request)
    {
        


        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Pranje toplom vodom";
        $cenovnik->cena_usluge="2e";
        $cenovnik->save();*/

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Poliranje";
        $cenovnik->cena_usluge="50e";
        $cenovnik->save();


        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Polimer zastita";
        $cenovnik->cena_usluge="25e";
        $cenovnik->save();


        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Voskiranje";
        $cenovnik->cena_usluge="5e";
        $cenovnik->save();


        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Sjaj guma";
        $cenovnik->cena_usluge="1.5e";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Usisavanje";
        $cenovnik->cena_usluge="2e";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Pranje";
        $cenovnik->usluga="Dubinsko usisavanje";
        $cenovnik->cena_usluge="50e";
        $cenovnik->save();

        
        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Veliki servis";
        $cenovnik->cena_usluge="50e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena delova trapa";
        $cenovnik->cena_usluge="20e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();
            
        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Mali servis";
        $cenovnik->cena_usluge="12e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena alternatora";
        $cenovnik->cena_usluge="15e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena seta kvacila";
        $cenovnik->cena_usluge="50e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Ravnjane glave i stelovanje ventila";
        $cenovnik->cena_usluge="35e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena hladnjaka vode";
        $cenovnik->cena_usluge="20e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena kocionih diskova i plocica";
        $cenovnik->cena_usluge="15e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena termostata";
        $cenovnik->cena_usluge="15e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena kocionog ulja";
        $cenovnik->cena_usluge="10e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena rashladne tecnosti";
        $cenovnik->cena_usluge="10e";
        $cenovnik->kategorija = "Mehanicki Radovi";
        $cenovnik->save();
        
        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena sijelice";
        $cenovnik->cena_usluge="2e";
        $cenovnik->kategorija = "Elektro Radovi";
        $cenovnik->save();
        
        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena motora brisaca";
        $cenovnik->cena_usluge="10e";
        $cenovnik->kategorija = "Elektro Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Zamena elektro podizaca stakala";
        $cenovnik->cena_usluge="10e";
        $cenovnik->kategorija = "Elektro Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Auto dijagnostika";
        $cenovnik->cena_usluge="15e";
        $cenovnik->kategorija = "Elektro Radovi";
        $cenovnik->save();

        $cenovnik = new Cenovnik;
        $cenovnik->pranje_ili_servis ="Servis";
        $cenovnik->usluga="Brisanje gresaka";
        $cenovnik->cena_usluge="10e";
        $cenovnik->kategorija = "Elektro Radovi";
        $cenovnik->save();
            
        $vozilo = new VoziloZaIznajmljivanje();
        $vozilo->naziv_vozila="BMW 120d";
        $vozilo->broj_vrata= 5;
        $vozilo->broj_sedista= 5;
        $vozilo->zapremina= 2000;
        $vozilo->automatski_ili_rucni_menjac="Automatski menjac"; 
        $vozilo->cena_po_danu= 25;
        $vozilo->kapacitet_prtljaznika="270";
        $vozilo->vrsta_goriva= "Dizel";
        $vozilo->sediste_za_dete= false;
        $vozilo->vrsta_vozila= "Hecbek";
        $vozilo->gps= false;
        $vozilo->klima= true;
        $vozilo->audio_plejer_ili_multimedija= "Audio_plejer";
        $vozilo->bluetooth= true;
        $vozilo->broj_airbagova=4;
        $vozilo->daljinsko_centralno_zakljucavanje= true;
        $vozilo->minibar= false;
        $vozilo->godina_proizvodnje="2005";
        $vozilo->save();

        $vozilo = new VoziloZaIznajmljivanje();
        $vozilo->naziv_vozila="BMW 320d";
        $vozilo->broj_vrata= 5;
        $vozilo->broj_sedista= 5;
        $vozilo->zapremina= 2000;
        $vozilo->automatski_ili_rucni_menjac="Manuelni menjac"; 
        $vozilo->cena_po_danu= 30;
        $vozilo->kapacitet_prtljaznika="340";
        $vozilo->vrsta_goriva= "Dizel";
        $vozilo->sediste_za_dete= false;
        $vozilo->vrsta_vozila= "Limuzina";
        $vozilo->gps= false;
        $vozilo->klima= true;
        $vozilo->audio_plejer_ili_multimedija= "Audio_plejer";
        $vozilo->bluetooth= true;
        $vozilo->broj_airbagova=4;
        $vozilo->daljinsko_centralno_zakljucavanje= true;
        $vozilo->minibar= false;
        $vozilo->godina_proizvodnje="2010";
        $vozilo->save();

        $vozilo = new VoziloZaIznajmljivanje();
        $vozilo->naziv_vozila="Mercedes Benz S500";
        $vozilo->broj_vrata= 5;
        $vozilo->broj_sedista= 5;
        $vozilo->zapremina= 5000;
        $vozilo->automatski_ili_rucni_menjac="Automatski menjac"; 
        $vozilo->cena_po_danu= 50;
        $vozilo->kapacitet_prtljaznika="420";
        $vozilo->vrsta_goriva= "Benzin";
        $vozilo->sediste_za_dete= false;
        $vozilo->vrsta_vozila= "Limuzina";
        $vozilo->gps= true;
        $vozilo->klima= true;
        $vozilo->audio_plejer_ili_multimedija= "Multimedija";
        $vozilo->bluetooth= true;
        $vozilo->broj_airbagova=8;
        $vozilo->daljinsko_centralno_zakljucavanje= true;
        $vozilo->minibar= true;
        $vozilo->godina_proizvodnje="2009";
        $vozilo->save();
        
        

    	$serviser = new Korisnik();
        $serviser->email_adresa = 'serviser123@gmail.com';
        $serviser->lozinka = 'JaSamServiser123!';
        $serviser->kontakt_telefon = '06154785214';
        $serviser->tip_korisnika = "Radnik";
        $serviser->tip_radnika = "Serviser";
        $serviser->save();

        $radnikUPerionici = new Korisnik();
        $radnikUPerionici->email_adresa = 'perac123@gmail.com';
        $radnikUPerionici->lozinka = "JaSamPerac123!";
        $radnikUPerionici->kontakt_telefon = '0652147852';
        $radnikUPerionici->tip_korisnika = "Radnik";
        $radnikUPerionici->tip_radnika = "Radnik u perionici";
        $radnikUPerionici->save();

        $noviKorisnik = new Korisnik();
        $noviKorisnik->email_adresa = "radnik1@gmail.com";
        $noviKorisnik->lozinka = "jaSamRadnik123";
        $noviKorisnik->kontakt_telefon = '0601';
        $noviKorisnik->tip_korisnika = "Radnik";
        $noviKorisnik->tip_radnika = "Radnik u perionici";
        $noviKorisnik->save();

        $noviKorisnik = new Korisnik();
        $noviKorisnik->email_adresa = "radnik2@gmail.com";
        $noviKorisnik->lozinka = "jaSamRadnik123";
        $noviKorisnik->kontakt_telefon = '0602';
        $noviKorisnik->tip_korisnika = "Radnik";
        $noviKorisnik->tip_radnika = "Serviser";
        $noviKorisnik->save();

        echo    "Uspesna dodela!!!";
    } 
}
