<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Korisnik;
use App\Zakazivanje;

class ZakazivanjeController extends Controller
{
    public function proveriDaLiJeDatumRealan($datum)
    {
        $dDatum = (new \DateTime($datum));
        $danasnjiDatum = (new \DateTime("now"));
        if($dDatum > $danasnjiDatum)
            return true;
        return false;
    }

    public function proveriDaLiJeDatumSlobodan($tipRezervacije, $datum)
    {
        if(!is_null(Zakazivanje::where('tip_rezervacije', $tipRezervacije)->where('zakazani_datum', $datum)->first()))
            return false;
        return true;
    }

    public function zakaziServisiranje(Request $request)
    {
    	$json = $_POST;
    	if($request->session()->has('servis') && $this->proveriDaLiJeDatumSlobodan("Servis", $json['zakazani_datum']) && $this->proveriDaLiJeDatumRealan($json['zakazani_datum']))
    	{
    		$rezervacija = new Zakazivanje();
    		$rezervacija->email_korisnika = $request->session()->get('servis');
    		$rezervacija->vozilo = $json['registraciona_oznaka'];
    		$rezervacija->zakazani_datum = $json['zakazani_datum'];
    		$rezervacija->tip_rezervacije = "Servis";
    		$rezervacija->vrsta_radova = $json['vrsta_radova'];
    		$rezervacija->opis_problema = $json['opis_problema'];
    		$rezervacija->save();
    		return response()->json(['Status' => "Uspesno je zakazano servisiranje!"]);
    	}
    	return response()->json(['Status' => 'Servisiranje nije uspesno zakazano!']);
    }

    public function zakaziServisiranjeKaoRadnik(Request $request)
    {
        $json = $_POST;
        if(/*$request->session()->has('servis') && */$this->proveriDaLiJeDatumSlobodan("Servis", $json['zakazani_datum']) && $this->proveriDaLiJeDatumRealan($json['zakazani_datum']))
        {
            $rezervacija = new Zakazivanje();
            $rezervacija->email_korisnika = $json['email_adresa'];
            $rezervacija->vozilo = $json['registraciona_oznaka'];
            $rezervacija->zakazani_datum = $json['zakazani_datum'];
            $rezervacija->tip_rezervacije = "Servis";
            $rezervacija->vrsta_radova = $json['vrsta_radova'];
            $rezervacija->opis_problema = $json['opis_problema'];
            $rezervacija->save();
            return response()->json(['Status' => "Uspesno je zakazano servisiranje!"]);
        }
        return response()->json(['Status' => 'Servisiranje nije uspesno zakazano!']);
    }

    public function zakaziPranje(Request $request)
    {
    	$json = $_POST;
    	if($request->session()->has('servis') && $this->proveriDaLiJeDatumSlobodan("Perionica", $json['zakazani_datum']) && $this->proveriDaLiJeDatumRealan($json['zakazani_datum']))
    	{
    		$rezervacija = new Zakazivanje();
    		$rezervacija->email_korisnika = $request->session()->get('servis');
    		$rezervacija->vozilo = $json['registraciona_oznaka'];
    		$rezervacija->zakazani_datum = $json['zakazani_datum'];
    		$rezervacija->tip_rezervacije = "Perionica";
    		$rezervacija->vrsta_pranja = $json['vrsta_pranja'];
    		$rezervacija->save();
    		return response()->json(['Status' => "Uspesno zakazano pranje!"]);
    	}
    	return response()->json(['Status' => "Pranje nije uspesno zakazano!"]);
    }

    public function zakaziPranjeKaoRadnik(Request $request)
    {
        $json = $_POST;
        if($request->session()->has('servis') && $this->proveriDaLiJeDatumSlobodan("Perionica", $json['zakazani_datum']) && $this->proveriDaLiJeDatumRealan($json['zakazani_datum']))
        {
            $rezervacija = new Zakazivanje();
            $rezervacija->email_korisnika = $json['email_korisnika'];
            $rezervacija->vozilo = $json['registraciona_oznaka'];
            $rezervacija->zakazani_datum = $json['zakazani_datum'];
            $rezervacija->tip_rezervacije = "Perionica";
            $rezervacija->vrsta_pranja = $json['vrsta_pranja'];
            $rezervacija->save();
            return response()->json(['Status' => "Uspesno zakazano pranje!"]);
        }
        return response()->json(['Status' => "Pranje nije uspesno zakazano!"]);
    }

    //FUNKCIJA KOJU KORISTI KORISNIK

    public function izlistajSveRezervacije(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	if($request->session()->has('servis'))
    	{
    		$i = 0;
    		if(!is_null(Zakazivanje::where('email_korisnika', $request->session()->get('servis'))->first()))
    		{
    			$sveRezervacijeKorisnika = Zakazivanje::where('email_korisnika', $request->session()->get('servis'))->get();
    			foreach($sveRezervacijeKorisnika as $value)
    			{
    				$slanje[$i]['tip_rezervacije'] = $value['tip_rezervacije'];
    				$slanje[$i]['registraciona_oznaka'] = $value['vozilo'];
    				$slanje[$i]['zakazani_datum'] = $value['zakazani_datum'];
    				$i = $i + 1;
    			}
    		}
    	}
    	return response()->json($slanje);
    }

    //FUNKCIJA KOJU KORISTI SERVISER

    public function izlistajSveRezervacijeZaServis(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        if(!is_null(Zakazivanje::where('tip_rezervacije', 'Servis')->first()))
        {
            $zakazivanjaZaServis = Zakazivanje::where('tip_rezervacije', 'Servis')->get();
            foreach($zakazivanjaZaServis as $value)
            {
                $slanje[$i]['id'] = $value['_id'];
                $slanje[$i]['email_korisnika'] = $value['email_korisnika'];
                $slanje[$i]['registraciona_oznaka'] = $value['vozilo'];
                $slanje[$i]['zakazani_datum'] = $value['zakazani_datum'];
                $slanje[$i]['vrsta_radova'] = $value['vrsta_radova'];
                $slanje[$i]['opis_problema'] = $value['opis_problema'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }

    //FUNKCIJA KOJU KORISTI PERAC

    public function izlistajSveRezervacijeZaPranje(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        if(!is_null(Zakazivanje::where('tip_rezervacije', 'Perionica')->first()))
        {
            $zakazivanjaZaPerionicu = Zakazivanje::where('tip_rezervacije', 'Perionica')->get();
            foreach($zakazivanjaZaPerionicu as $value)
            {
                $slanje[$i]['id'] = $value['_id'];
                $slanje[$i]['email_korisnika'] = $value['email_korisnika'];
                $slanje[$i]['registraciona_oznaka'] = $value['vozilo'];
                $slanje[$i]['zakazani_datum'] = $value['zakazani_datum'];
                $slanje[$i]['vrsta_pranja'] = $value['vrsta_pranja'];
                $i = $i + 1;
            }
        }

        return response()->json($slanje);
    }


    public function obrisiVoziloIzListeZakazanihVozilaZaServisiranje(Request $request)
    {
        $json = $_POST;
        $zakazivanje = Zakazivanje::find($json['id']);
        $zakazivanje->delete();
        return response()->json(['Status' => "Uspesno obrisano vozilo iz liste zakazanih vozila za servis!"]);
    }

    public function obrisiVoziloIzListeZakazanihVozilaZaPerionicu(Request $request)
    {
        $json = $_POST;
        $zakazivanje = Zakazivanje::find($json['id']);
        $zakazivanje->delete();
        if(is_null(Zakazivanje::find($json['id'])))
        {
            return response()->json(['Status' => "Uspesno obrisano vozilo iz liste zakazanih vozila za perionicu!"]);
        }
        else
        {
            return response()->json(['Status' => "Nije uspela aktivnost!"]);
        }
    }

    public function obrisiVoziloZaServisiranje(Request $request)
    {
        $json = $_POST;
        $zakazivanje = Zakazivanje::where('email_korisnika', $json['email_adresa'])->where('vozilo', $json['registraciona_oznaka'])->first();
        if(!is_null($zakazivanje))
        {
            $zakazivanje->delete();
            if(is_null(Zakazivanje::where('email_korisnika', $json['email_adresa'])->where('vozilo', $json['registraciona_oznaka'])->first()))
            {
                return response()->json(['Status' => "Uspesno obrisano vozilo iz liste zakazanih vozila za perionicu!"]);
            }
        }
        return response()->json(['Status' => "Nije uspela aktivnost!"]);
    }

    
}
