<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Cenovnik extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'cenovnik';
    protected $fillable = ['pranje_ili_servis', 'kategorija', 'usluga', 'cena_usluge'];
}
