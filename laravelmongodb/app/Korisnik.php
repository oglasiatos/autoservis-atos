<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Korisnik extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'korisnik';

    protected $fillable = [ 'email_adresa',
							'lozinka',
							'kontakt_telefon',
							'tip_korisnika',
							'tip_radnika',
							'vozila'
						  ];
}
