<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class VoziloZaIznajmljivanje extends Model
{
	protected $connection = 'mongodb';
    protected $collection = 'vozilo_za_iznajmljivanje';

    protected $fillable = [ 'naziv_vozila',
						    'broj_vrata', 
						    'broj_sedista', 
						    'zapremina', 
							'automatski_ili_rucni_menjac', 
							'cena_po_danu', 
							'kapacitet_prtljaznika', 
							'vrsta_goriva', 
							'sediste_za_dete', 
							'vrsta_vozila', 
							'gps', 
							'klima', 
							'audio_plejer_ili_multimedija', 
							'bluetooth', 
							'broj_airbagova', 
							'daljinsko_centralno_zakljucavanje', 
							'minibar', 
							'godina_proizvodnje',
							'lista_rezervacija'];
}
